package org.sklsft.connect.client.authentication;

import javax.inject.Inject;

import org.sklsft.commons.rest.client.RestClient;
import org.sklsft.connect.api.interfaces.authentication.AuthenticationService;
import org.sklsft.connect.api.model.authentication.of.LoginForm;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.springframework.stereotype.Service;


/**
 * this service provides a token given a {@link LoginForm}.<br/>
 * this token will be necessary for calling the majority of rest services
 * @author Nicolas Thibault
 *
 */
@Service
public class AuthenticationServiceClient implements AuthenticationService {

	@Inject
	private RestClient restClient;
	


	@Override
	public TokenView authenticate(LoginForm form) {
		 return restClient.postForObject(AuthenticationService.AUTHENTICATE_URL, form, TokenView.class);
	}
}
