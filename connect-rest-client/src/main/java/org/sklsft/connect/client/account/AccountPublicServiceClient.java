package org.sklsft.connect.client.account;

import javax.inject.Inject;

import org.sklsft.commons.rest.client.RestClient;
import org.sklsft.connect.api.interfaces.account.AccountPublicService;
import org.springframework.stereotype.Service;


@Service
public class AccountPublicServiceClient implements AccountPublicService {
	
	@Inject
	private RestClient restClient;
	
	
	
	@Override
	public byte[] getAccountLogo(Long id) {
		// directly provided by rest api
		return null;
	}
}
