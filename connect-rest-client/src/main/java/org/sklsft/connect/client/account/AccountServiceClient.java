package org.sklsft.connect.client.account;

import org.sklsft.commons.rest.client.RestClient;
import org.sklsft.connect.api.exception.account.EmailAlreadyActivatedException;
import org.sklsft.connect.api.exception.account.ValidationEmailExpiredException;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.account.of.*;
import org.sklsft.connect.api.model.account.ov.AccountBasicView;
import org.sklsft.connect.api.model.account.ov.AccountFullView;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.social.api.exception.state.account.AccountAlreadyUsedException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Service
public class AccountServiceClient implements AccountService {

	@Inject
	private RestClient restClient;

	@Override
	public TokenView createAccount(AccountCreationForm form)
			throws AccountAlreadyUsedException {
		Map<String, String> params = new HashMap<String, String>();
		//params.put("Content-Type", "application/json,application/xml");
		params.put("Accept", "application/html");
		return restClient.postForObject(CREATE_ACCOUNT_URL, form,
				TokenView.class,params);
	}
	

	@Override
	public TokenView activateEmail(String token)
			throws EmailAlreadyActivatedException,
			ValidationEmailExpiredException {
		return restClient.postForObject(ACTIVATE_EMAIL_URL, token,
				TokenView.class);
	}
	

	@Override
	public void resendValidationMail() {
		restClient.getForObject(RESEND_VALIDATION_EMAIL_URL, Void.class);

	}
	

	@Override
	public AccountBasicView retrieveMyAccount() {
		return restClient.getForObject(RETRIEVE_MY_ACCOUNT_URL,
				AccountBasicView.class);
	}
	
	
	@Override
	public AccountFullView retrieveMyFullAccount() {
		return restClient.getForObject(RETRIEVE_MY_FULL_ACCOUNT_URL,
				AccountFullView.class);
	}
	

	@Override
	public void updateMyPersonalData(PersonalDataForm form) {
		restClient.put(UPDATE_MY_PERSONNAL_DATA_URL, form);

	}
	

	@Override
	public void updateMyMail(MailForm form) {
		restClient.put(UPDATE_MY_EMAIL_URL, form);
	}


	@Override
	public void updateMyPassword(AccountPasswordForm accountPasswordForm) {
		restClient.put(UPDATE_MY_PASSWORD_URL, accountPasswordForm);
	}
	

	@Override
	public void uploadMyAccountLogo(byte[] data) {
		restClient.put(UPLOAD_MY_LOGO_URL, data);

	}


	@Override
	public void sendResetPassword(MailForm emailForm) {
		restClient.put(SEND_RESET_PASSWORD_URL, emailForm);
	}


	@Override
	public TokenView resetPassword(ResetPasswordForm resetPasswordForm) {
		return restClient.postForObject(RESET_PASSWORD_URL,
				resetPasswordForm, TokenView.class);
	}
}
