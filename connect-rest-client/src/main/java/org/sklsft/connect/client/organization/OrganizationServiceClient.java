package org.sklsft.connect.client.organization;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.sklsft.commons.rest.client.RestClient;
import org.sklsft.connect.api.interfaces.organization.OrganizationService;
import org.sklsft.connect.api.model.organization.of.OrganizationCodeForm;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.sklsft.connect.api.model.organization.ov.InvitationView;
import org.sklsft.connect.api.model.organization.ov.OrganizationBasicView;
import org.sklsft.connect.api.model.organization.ov.OrganizationFullView;
import org.springframework.stereotype.Service;

@Service
public class OrganizationServiceClient implements OrganizationService {

	@Inject
	private RestClient restClient;
	
	
	@Override
	public List<OrganizationBasicView> retrieveMyOrganizations() {
		return Arrays.asList(restClient.getForObject(OrganizationService.GET_MY_ORGANIZATION_LIST_URL, OrganizationBasicView[].class));
	}
	
	
	@Override
	public List<InvitationView> retrieveMyInvitations() {
		return Arrays.asList(restClient.getForObject(OrganizationService.GET_MY_INVITATION_LIST_URL, InvitationView[].class));
	}
	
	
	@Override
	public OrganizationFullView retrieveMyOrganization(Long id) {
		
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("id", id);
		
		return restClient.getForObject(OrganizationService.GET_MY_ORGANIZATION_URL, OrganizationFullView.class, vars);
	}
	
	
	@Override
	public boolean existsOrganization(String code) {
		
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("code", code);
		
		return restClient.postForObject(OrganizationService.EXISTS_ORGANIZATION_URL, code, Boolean.class, vars);
	}
	

	@Override
	public Long createOrganization(OrganizationForm form) {
		
		return restClient.postForObject(OrganizationService.CREATE_ORGANIZATION_URL, form, Long.class);		
	}
	

	@Override
	public void updateMyOrganization(Long id, OrganizationForm form) {
		
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("id", id);
		
		restClient.put(OrganizationService.UPDATE_MY_ORGANIZATION_URL, form, vars);
	}


	@Override
	public void updateMyOrganizationIdentifier(Long id, OrganizationCodeForm organizationCodeForm) {
		
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("id", id);
		
		restClient.put(OrganizationService.UPDATE_MY_ORGANIZATION_IDENTIFIER_URL, organizationCodeForm, vars);
		
	}
	

	@Override
	public void uploadMyOrganizationLogo(Long id, byte[] bytes) {
		
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("id", id);
		
		restClient.put(OrganizationService.UPLOAD_MY_ORGANIZATION_LOGO_URL, bytes, vars);
		
	}


	@Override
	public void deleteMyOrganization(Long id) {
		
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("id", id);
		
		restClient.delete(OrganizationService.DELETE_MY_ORGANIZATION_URL, vars);
	}
}
