package org.sklsft.connect.client.organization;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.sklsft.commons.rest.client.RestClient;
import org.sklsft.connect.api.interfaces.organization.OrganizationMembershipService;
import org.sklsft.connect.api.model.organization.of.GuestForm;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.connect.api.model.organization.ov.GuestView;
import org.sklsft.connect.api.model.organization.ov.MemberView;
import org.springframework.stereotype.Service;

@Service
public class OrganizationMembershipServiceClient implements OrganizationMembershipService {

	@Inject
	private RestClient restClient;
	

	@Override
	public List<MemberView> getMyOrganizationMembers(Long id) {
		
		Map<String, Object> vars = new HashMap<>();
		vars.put("id", id);
		
		return Arrays.asList(restClient.getForObject(GET_MY_ORGANIZATION_MEMBERS_URL, MemberView[].class, vars));
	}
	

	@Override
	public void updateMember(Long organizationId, Long accountId, MemberForm form) {
		
		Map<String, Object> vars = new HashMap<>();
		vars.put("organizationId", organizationId);
		vars.put("accountId", accountId);

		restClient.put(UPDATE_MY_ORGANIZATION_MEMBER_URL, form, vars);

	}
	

	@Override
	public void deleteMember(Long organizationId, Long accountId) {
		
		Map<String, Object> vars = new HashMap<>();
		vars.put("organizationId", organizationId);
		vars.put("accountId", accountId);
		
		restClient.delete(DELETE_MY_ORGANIZATION_MEMBER_URL, vars);

	}
	
	
	@Override
	public List<GuestView> getMyOrganizationGuests(Long id) {
		
		Map<String, Object> vars = new HashMap<>();
		vars.put("id", id);
		
		return Arrays.asList(restClient.getForObject(GET_MY_ORGANIZATION_GUESTS_URL, GuestView[].class, vars));
	}
	

	@Override
	public void inviteMember(Long id, GuestForm form) {

		Map<String, Object> vars = new HashMap<>();
		vars.put("id", id);
		
		restClient.put(INVITE_MEMBER_URL, form, vars);
	}
	

	@Override
	public void cancelInviteMember(Long id, String email) {
		
		Map<String, Object> vars = new HashMap<>();
		vars.put("id", id);
		vars.put("email", email);
		
		restClient.delete(CANCEL_INVITE_MEMBER_URL, vars);
	}
	

	@Override
	public void acceptJoinOrganization(Long id) {
		
		Map<String, Object> vars = new HashMap<>();
		vars.put("id", id);
		
		restClient.put(ACCEPT_INVITATION_URL, null, vars);

	}
	

	@Override
	public void refuseJoinOrganization(Long id) {
		
		Map<String, Object> vars = new HashMap<>();
		vars.put("id", id);
		
		restClient.put(REFUSE_INVITATION_URL, null, vars);

	}
	

	@Override
	public void leaveOrganization(Long id) {
		
		Map<String, Object> vars = new HashMap<>();
		vars.put("id", id);
		
		restClient.put(LEAVE_ORGANIZATION_URL, null, vars);

	}
}
