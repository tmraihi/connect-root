package org.sklsft.connect.client.file;

import javax.inject.Inject;

import org.sklsft.commons.rest.client.RestClient;
import org.springframework.stereotype.Service;

@Service
public class FileService {

	@Inject
	private RestClient restClient;
	
	
	public byte[] getFileContent(String relativeUrl) {
		return restClient.getForObject(relativeUrl, byte[].class);
	}

}
