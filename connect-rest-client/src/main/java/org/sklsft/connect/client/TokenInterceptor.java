package org.sklsft.connect.client;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;


public class TokenInterceptor implements ClientHttpRequestInterceptor {
	
	private static final Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);
	
	private volatile String licenceKey;

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

    	HttpHeaders headers = request.getHeaders();
    	String token = getToken();
    	if (token != null) {
    		headers.add("token", token);
    	}
    	
    	String licenceKey = getLicenceKey();
    	if (licenceKey != null) {
    		headers.add("licence", licenceKey);
    	}
    	
    	return execution.execute(request, body);
    }
    

	/**
	 * provides the token given by the spring security context
	 */
	private String getToken() {
		
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			if (!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
				return ((TokenView) SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal()).getToken();
			}
		}
		return null;
	}
	
	
	/**
	 * provides the licence key from JNDI
	 */
	private String getLicenceKey() {
		
		if (this.licenceKey == null) {		
			try {
				InitialContext initialContext = new InitialContext();
				Context environmentContext = (Context)initialContext.lookup("java:/comp/env");
				String licenceKey = "licenceTao";//(String) environmentContext.lookup("connectLicenceKey");
				this.licenceKey = licenceKey;
			} catch (NamingException e) {
				logger.warn("failed to find licence key in JNDI : " + e.getMessage(), e);
			}
		}
		return this.licenceKey;
	}
}
