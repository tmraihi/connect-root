package org.sklsft.connect.client.organization;

import org.sklsft.connect.api.interfaces.organization.OrganizationPublicService;
import org.springframework.stereotype.Service;

@Service
public class OrganizationPublicServiceClient implements OrganizationPublicService {

	
	@Override
	public byte[] getOrganizationLogo(Long id) {
		//directly provided by rest api
		return null;
	}
}
