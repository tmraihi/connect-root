package org.sklsft.connect.junit.populator;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.sklsft.generator.bl.services.impl.JunitPopulator;
import org.sklsft.generator.bl.services.interfaces.ProjectLoader;
import org.sklsft.generator.bl.services.interfaces.ProjectMetaDataService;
import org.sklsft.generator.model.domain.Project;
import org.sklsft.generator.model.metadata.ProjectMetaData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JunitDataInitializer {

	/*
	 * logger
	 */
	private final Logger logger = LoggerFactory.getLogger(JunitDataInitializer.class);
	
	@Inject
	private DataSource dataSource;

	@Inject
	private JunitPopulator populator;
	
	@Inject
	private ProjectMetaDataService projectMetaDataService;
	
	@Inject
	private ProjectLoader projectLoader;

	
	private boolean initialized = false;
	Project project;
	
	private static final String TRUNCATE_SHEMA_SCRIPT = "TRUNCATE SCHEMA PUBLIC RESTART IDENTITY AND COMMIT NO CHECK";
	private static final String CREATE_UNACCENT_FUNCTION = "CREATE FUNCTION normalize(CHAR VARYING(255))"
																		+ " RETURNS CHAR VARYING(255)"
																		+ " LANGUAGE JAVA DETERMINISTIC NO SQL"
																		+ " EXTERNAL NAME 'CLASSPATH:org.sklsft.commons.text.StringUtils.normalize'";

	public void initialize(String... packages) {

		if (initialized) {
			purgeDatabase();
		} else {
			createUnaccentFunction();
			logger.info("start loading project");
			ProjectMetaData projectMetaData = projectMetaDataService.loadProjectMetaData("../../../skeleton-social/social-root");
			project = projectLoader.loadProject(projectMetaData);
			logger.info("loading project " + project.projectName + " completed");
		}
		
		logger.info("Initializing data");
		
		Set<String> packs = new HashSet<>(Arrays.asList(packages));
				
		populator.populate(project, packs, "src/test/resources/junit/data/social");
		
		logger.info("completed");

		initialized = true;
	}

	private void purgeDatabase() {

		try (Connection connection = dataSource.getConnection(); Statement statement = connection.createStatement();) {			
			statement.execute(TRUNCATE_SHEMA_SCRIPT);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	private void createUnaccentFunction() {

		try (Connection connection = dataSource.getConnection(); Statement statement = connection.createStatement();) {			
			statement.execute(CREATE_UNACCENT_FUNCTION);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
