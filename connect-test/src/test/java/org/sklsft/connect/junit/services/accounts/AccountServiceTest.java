package org.sklsft.connect.junit.services.accounts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.sklsft.commons.rest.security.context.SecurityContextHolder;
import org.sklsft.commons.rest.security.context.SecurityContextProvider;
import org.sklsft.connect.api.exception.account.EmailAlreadyActivatedException;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.account.of.AccountCreationForm;
import org.sklsft.connect.api.model.account.of.MailForm;
import org.sklsft.connect.api.model.account.of.PersonalDataForm;
import org.sklsft.connect.api.model.account.ov.AccountBasicView;
import org.sklsft.connect.api.model.account.ov.AccountFullView;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.junit.populator.JunitDataInitializer;
import org.sklsft.social.api.exception.state.account.AccountAlreadyUsedException;
import org.sklsft.social.api.interfaces.account.UserAccountService;
import org.sklsft.social.api.model.account.forms.UserAccountForm;
import org.sklsft.social.api.model.account.views.full.UserAccountFullView;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-connect-test.xml" })
public class AccountServiceTest {
	
	private static final String NEW_MAIL = "new@email.com";
	private static final String NEW_FIRST_NAME = "firstName";
	private static final String NEW_LAST_NAME = "lastName";
	private static final String NEW_PASSWORD = "password";

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Inject
	private JunitDataInitializer dataInitializer;

	@Inject
	private SecurityContextProvider securityContextProvider;

	@Inject
	private AccountService accountService;

	@Inject
	private UserAccountService userAccountService;

	@Before
	public void setUp() {
		dataInitializer.initialize();
	}

	@After
	public void tearDown() {
		SecurityContextHolder.unbindCredentials();
	}
	
	
	@Test
	public void testCreateAccount() {

		int nbAccounts = userAccountService.loadList().size();

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		accountService.createAccount(accountCreationForm);

		assertEquals(nbAccounts + 1, userAccountService.loadList().size());

		UserAccountForm userAccount = userAccountService.find(NEW_MAIL).getForm();

		assertEquals(NEW_FIRST_NAME, userAccount.getFirstName());
		assertEquals(NEW_LAST_NAME, userAccount.getLastName());
	}
	

	@Test
	public void testCreateAccountAccountAlreadyUsedException() {

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		accountService.createAccount(accountCreationForm);

		exception.expect(AccountAlreadyUsedException.class);
		exception.expectMessage("account.alreadyUsed");

		accountService.createAccount(accountCreationForm);

	}
	

	@Test
	public void testActivateEmail() {

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		TokenView tokenView = accountService.createAccount(accountCreationForm);

		UserAccountFullView userAccount = userAccountService.find(NEW_MAIL);

		assertFalse(userAccount.getForm().getEmailValidated());

		accountService.activateEmail(tokenView.getToken());

		userAccount = userAccountService.find(NEW_MAIL);

		assertTrue(userAccount.getForm().getEmailValidated());
	}
	

	@Test
	public void testActivateEmailEmailAlreadyActivatedException() {

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		TokenView tokenView = accountService.createAccount(accountCreationForm);

		accountService.activateEmail(tokenView.getToken());

		exception.expect(EmailAlreadyActivatedException.class);
		exception.expectMessage("account.alreadyValidated");

		accountService.activateEmail(tokenView.getToken());

	}
	

	@Test
	public void testRetriveMyAccount() {

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		TokenView tokenView = accountService.createAccount(accountCreationForm);
		
		securityContextProvider.provideSecurityContext(tokenView.getToken());

		AccountBasicView accountBasicView = accountService.retrieveMyAccount();

		UserAccountFullView fullView = userAccountService.find(NEW_MAIL);
		
		assertEquals(fullView.getForm().getFirstName(), accountBasicView.getFirstName());
		assertEquals(fullView.getForm().getLastName(), accountBasicView.getLastName());

	}
	

	@Test
	public void testRetriveMyFullAccount() {

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		TokenView tokenView = accountService.createAccount(accountCreationForm);
		
		securityContextProvider.provideSecurityContext(tokenView.getToken());

		AccountFullView accountFullView = accountService.retrieveMyFullAccount();

		UserAccountFullView fullView = userAccountService.find(NEW_MAIL);
		
		assertEquals(fullView.getForm().getFirstName(), accountFullView.getPersonalData().getFirstName());
		assertEquals(fullView.getForm().getLastName(), accountFullView.getPersonalData().getLastName());
		assertEquals(fullView.getForm().getEmail(), accountFullView.getEmailForm().getEmail());

	}
	

	@Test
	public void testUpdateMyPersonalData() {
		
		String UPDATED_FIRST_NAME = "updatedFirstName";
		String UPDATED_LAST_NAME = "updatedLastName";

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		TokenView tokenView = accountService.createAccount(accountCreationForm);
		
		securityContextProvider.provideSecurityContext(tokenView.getToken());
		
		AccountFullView accountFullView = accountService.retrieveMyFullAccount();
		PersonalDataForm form = accountFullView.getPersonalData();
		
		form.setFirstName(UPDATED_FIRST_NAME);
		form.setLastName(UPDATED_LAST_NAME);
		
		accountService.updateMyPersonalData(form);
		
		accountFullView = accountService.retrieveMyFullAccount();
		
		assertEquals(UPDATED_FIRST_NAME, accountFullView.getPersonalData().getFirstName());
		assertEquals(UPDATED_LAST_NAME, accountFullView.getPersonalData().getLastName());
	}
	

	@Test
	public void testUpdateMyMail() {

		String UPDATED_MAIL = "updated@email.com";

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		TokenView tokenView = accountService.createAccount(accountCreationForm);
		
		securityContextProvider.provideSecurityContext(tokenView.getToken());
		
		AccountFullView accountFullView = accountService.retrieveMyFullAccount();
		MailForm form = accountFullView.getEmailForm();
		
		form.setEmail(UPDATED_MAIL);
		
		accountService.updateMyMail(form);
		
		accountFullView = accountService.retrieveMyFullAccount();
		
		assertEquals(UPDATED_MAIL, accountFullView.getEmailForm().getEmail());

		AccountBasicView accountBasicView = accountService.retrieveMyAccount();

		assertFalse(accountBasicView.getEmailValidated());

	}


	@Test
	public void testUpdateMyMailAccountAlreadyUsedException() {

		String ALREADY_USED_MAIL = "used@email.com";

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL);

		TokenView tokenView = accountService.createAccount(accountCreationForm);
		
		accountCreationForm = provideValidAccountCreationForm(ALREADY_USED_MAIL);

		tokenView = accountService.createAccount(accountCreationForm);
		
		securityContextProvider.provideSecurityContext(tokenView.getToken());
		
		AccountFullView accountFullView = accountService.retrieveMyFullAccount();
		MailForm form = accountFullView.getEmailForm();
		
		form.setEmail(ALREADY_USED_MAIL);
		
		accountService.updateMyMail(form);
	}


	private PersonalDataForm provideValidDataForm() {
		PersonalDataForm form = new PersonalDataForm();
		form.setFirstName(NEW_FIRST_NAME);
		form.setLastName(NEW_LAST_NAME);

		return form;
	}
	

	private AccountCreationForm provideValidAccountCreationForm(String userEmail) {

		AccountCreationForm accountCreationForm = new AccountCreationForm();
		accountCreationForm.setEmail(userEmail);
		accountCreationForm.setPassword(NEW_PASSWORD);
		accountCreationForm.setPersonalData(provideValidDataForm());

		return accountCreationForm;
	}
}
