package org.sklsft.connect.junit.security;

import java.util.Date;

import javax.inject.Inject;

import org.sklsft.commons.rest.security.tokens.encoder.TokenEncoder;
import org.sklsft.connect.bc.security.credentials.UserSecurityCredentials;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.springframework.transaction.annotation.Transactional;


public class TokenEncoderMock implements TokenEncoder<UserSecurityCredentials> {

	@Inject
	private UserAccountDao userAccountDao;



	@Override
	@Transactional
	public UserSecurityCredentials decode(String token) {
		
		UserSecurityCredentials securityCredentials = new UserSecurityCredentials();

		UserAccount user = userAccountDao.load(Long.valueOf(token));

		securityCredentials.setAccountId(user.getId());
		securityCredentials.setAccountCreationDate(user.getCreationDate());
		securityCredentials.setCreationDate(new Date());

		return securityCredentials;
	}


	@Override
	@Transactional
	public String encode(UserSecurityCredentials credentials) {

		return credentials.getAccountId().toString();
	}

}
