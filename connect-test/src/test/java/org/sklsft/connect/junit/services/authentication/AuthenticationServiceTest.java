package org.sklsft.connect.junit.services.authentication;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.sklsft.commons.rest.security.context.SecurityContextHolder;
import org.sklsft.connect.api.exception.authentication.AuthenticationException;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.interfaces.authentication.AuthenticationService;
import org.sklsft.connect.api.model.account.of.AccountCreationForm;
import org.sklsft.connect.api.model.account.of.PersonalDataForm;
import org.sklsft.connect.api.model.authentication.of.LoginForm;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.junit.populator.JunitDataInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-connect-test.xml" })
public class AuthenticationServiceTest {
	
	private static final String NEW_MAIL = "new@email.com";
	private static final String NEW_FIRST_NAME = "firstName";
	private static final String NEW_LAST_NAME = "lastName";
	private static final String NEW_PASSWORD = "password";

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Inject
	private JunitDataInitializer dataInitializer;

	@Inject
	private AuthenticationService authenticationService;

	@Inject
	private AccountService accountService;
	

	@Before
	public void setUp() {
		dataInitializer.initialize();
	}

	@After
	public void tearDown() {
		SecurityContextHolder.unbindCredentials();
	}

	@Test
	public void testAuthenticate() {

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL, NEW_PASSWORD);

		TokenView tokenView = accountService.createAccount(accountCreationForm);

		LoginForm loginForm = provideLoginForm(NEW_MAIL, NEW_PASSWORD);

		TokenView authenticateToken = authenticationService.authenticate(loginForm);

		assertEquals(tokenView.getAccountId(), authenticateToken.getAccountId());

	}

	@Test
	public void testAuthenticateBadPassword() {
		
		String BAD_PASSWORD = "badPassword";

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL, NEW_PASSWORD);

		accountService.createAccount(accountCreationForm);

		LoginForm loginForm = provideLoginForm(NEW_MAIL, BAD_PASSWORD);

		exception.expect(AuthenticationException.class);
		exception.expectMessage("account.authenticationFailure");
		authenticationService.authenticate(loginForm);

	}

	@Test
	public void testAuthenticateBadUser() {

		String BAD_EMAIL = "bad@email.com";

		AccountCreationForm accountCreationForm = provideValidAccountCreationForm(NEW_MAIL, NEW_PASSWORD);

		accountService.createAccount(accountCreationForm);

		LoginForm loginForm = provideLoginForm(BAD_EMAIL, NEW_PASSWORD);

		exception.expect(AuthenticationException.class);
		exception.expectMessage("account.authenticationFailure");
		authenticationService.authenticate(loginForm);

	}
	

	private AccountCreationForm provideValidAccountCreationForm(String userEmail, String userPassword) {

		AccountCreationForm accountCreationForm = new AccountCreationForm();
		accountCreationForm.setEmail(userEmail);
		accountCreationForm.setPassword(userPassword);

		PersonalDataForm personalData = new PersonalDataForm();
		personalData.setFirstName(NEW_FIRST_NAME);
		personalData.setLastName(NEW_LAST_NAME);

		accountCreationForm.setPersonalData(personalData);

		return accountCreationForm;
	}

	private LoginForm provideLoginForm(String login, String password) {
		LoginForm loginForm = new LoginForm();
		loginForm.setLogin(login);
		loginForm.setPassword(password);
		return loginForm;
	}
}
