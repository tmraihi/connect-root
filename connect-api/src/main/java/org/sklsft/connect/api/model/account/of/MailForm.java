package org.sklsft.connect.api.model.account.of;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.sklsft.connect.api.model.pattern.Patterns;

public class MailForm implements Serializable {

	private static final long serialVersionUID = 2817998148091067992L;
	
	/*
	 * properties
	 */
	@NotNull
	@Pattern(regexp = Patterns.EMAIL_PATTERN , message = "{mail.pattern.invalid}")
	private String email;
	
	
	/*
	 * getters and setters
	 */
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
