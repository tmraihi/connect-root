package org.sklsft.connect.api.exception.authentication;

import org.sklsft.commons.api.exception.ApplicationException;


/**
 * Exception thrown when the security aspect rejects the given token
 */
public class InvalidTokenException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	public InvalidTokenException(String message) {
		super(message);

	}

	public InvalidTokenException(String message, Throwable cause) {
		super(message, cause);
	}
}
