package org.sklsft.connect.api.model.organization.of;

import java.io.Serializable;

import org.sklsft.connect.api.model.account.of.MailForm;

public class GuestForm implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * properties
	 */
	private MailForm mailForm = new MailForm();
	private MemberForm memberForm = new MemberForm();
	
	
	/*
	 * getters and setters
	 */
	public MailForm getMailForm() {
		return mailForm;
	}
	public void setMailForm(MailForm mailForm) {
		this.mailForm = mailForm;
	}
	public MemberForm getMemberForm() {
		return memberForm;
	}
	public void setMemberForm(MemberForm memberForm) {
		this.memberForm = memberForm;
	}
}
