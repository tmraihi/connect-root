package org.sklsft.connect.api.model.account.of;


import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.sklsft.connect.api.model.pattern.Patterns;

public class AccountCreationForm implements Serializable {
	
	private static final long serialVersionUID = -5244761348841697168L;

	/*
	 * properties
	 */
	@NotNull
	@Pattern(regexp = Patterns.EMAIL_PATTERN , message = "{mail.pattern.invalid}")
	private String email;
	@NotNull
	@Size(min=Patterns.MIN_PASSWORD_LENGTH, max=Patterns.MAX_PASSWORD_LENGTH, message="{password.length.invalid}")
	private String password;
	
	private PersonalDataForm personalData = new PersonalDataForm();
	
	/*
	 * getters and setters
	 */
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public PersonalDataForm getPersonalData() {
		return personalData;
	}
	public void setPersonalData(PersonalDataForm personalData) {
		this.personalData = personalData;
	}
}
