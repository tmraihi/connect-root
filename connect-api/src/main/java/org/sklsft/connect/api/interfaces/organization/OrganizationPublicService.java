package org.sklsft.connect.api.interfaces.organization;



public interface OrganizationPublicService {

	
	/**
	 * Only necessary from the rest server point of view to expose the resource<br>
	 * The service will never be consummed : the resource will be accessed
	 * directly because an attachment view knows its url
	 */
	byte[] getOrganizationLogo(Long id);
	static final String GET_ORGANIZATION_LOGO_URL = "/organization/public/{id}/logo";
}
