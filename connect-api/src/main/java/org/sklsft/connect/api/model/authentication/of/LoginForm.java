package org.sklsft.connect.api.model.authentication.of;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.sklsft.connect.api.model.pattern.Patterns;

public class LoginForm implements Serializable {
	
	private static final long serialVersionUID = -4413134553593101500L;
	
	/*
	 * properties
	 */
	@NotEmpty(message="{field.mandatory}")
	private String login="taoufik.mraihi@gmail.com";
	
	@NotEmpty(message="{field.mandatory}")
	@Size(min=Patterns.MIN_PASSWORD_LENGTH, max=Patterns.MAX_PASSWORD_LENGTH, message="{password.length.invalid}")
	private String password="azertyuiop";
	
	
	/*
	 * getters and setters
	 */
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
