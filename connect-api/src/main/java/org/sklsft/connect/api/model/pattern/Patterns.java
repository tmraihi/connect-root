package org.sklsft.connect.api.model.pattern;

public interface Patterns {
	
	public static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public static final String SIRET_PATTERN = "^[0-9]{14}$";

	public static final String ORGANIZATION_CODE_PATTERN = "^[a-z0-9-]{1,255}$";
	
	public static final int MIN_PASSWORD_LENGTH=8;
	public static final int MAX_PASSWORD_LENGTH=100;
	

}
