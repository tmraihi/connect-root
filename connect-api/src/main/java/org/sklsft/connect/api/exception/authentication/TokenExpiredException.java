package org.sklsft.connect.api.exception.authentication;

import org.sklsft.commons.api.exception.ApplicationException;



/**
 * Exception thrown when the security aspect rejects the given token because of expiration
 */
public class TokenExpiredException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	public TokenExpiredException(String message) {
		super(message);

	}

	public TokenExpiredException(String message, Throwable cause) {
		super(message, cause);
	}
}
