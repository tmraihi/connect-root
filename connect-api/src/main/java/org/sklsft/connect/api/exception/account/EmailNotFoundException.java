package org.sklsft.connect.api.exception.account;

import org.sklsft.commons.api.exception.ApplicationException;


public class EmailNotFoundException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public EmailNotFoundException() {
		super();
	}
	
	public EmailNotFoundException(String message) {
		super(message);
	}
	
	public EmailNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
