package org.sklsft.connect.api.exception.authentication;

import org.sklsft.commons.api.exception.ApplicationException;


/**
 * Exception thrown when trying to give a token to a desactivated account
 *
 */
public class AccountLockedException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;

	
	public AccountLockedException() {
		super();

	}
	
	public AccountLockedException(String message) {
		super(message);

	}

	public AccountLockedException(String message, Throwable cause) {
		super(message, cause);
	}
}
