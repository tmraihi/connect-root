package org.sklsft.connect.api.exception.account;

import org.sklsft.commons.api.exception.ApplicationException;


/**
 * Exception thrown when the token sent by mail to validate the mail has expired
 */
public class ValidationEmailExpiredException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	
	public ValidationEmailExpiredException(){
		super();
	}
	
	public ValidationEmailExpiredException(String message) {
		super(message);

	}

	public ValidationEmailExpiredException(String message, Throwable cause) {
		super(message, cause);
	}
}
