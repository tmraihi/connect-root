package org.sklsft.connect.api.model.organization.of;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.sklsft.social.util.roles.OrganizationRole;

public class MemberForm implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * properties
	 */
	@NotNull
	private OrganizationRole role;
	private String position;
	
	
	/*
	 * getters and setters
	 */
	public OrganizationRole getRole() {
		return role;
	}
	public void setRole(OrganizationRole role) {
		this.role = role;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
}
