package org.sklsft.connect.api.interfaces.account;

public interface AccountPublicService {
	
	/**
	 * Only necessary from the rest server point of view to expose the resource<br>
	 * The service will never be consummed : the resource will be accessed
	 * directly because an attachment view knows its url
	 */
	byte[] getAccountLogo(Long id);
	static final String GET_ACCOUNT_LOGO_URL = "/account/{id}/logo";
}
