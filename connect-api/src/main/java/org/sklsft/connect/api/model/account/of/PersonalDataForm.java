package org.sklsft.connect.api.model.account.of;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class PersonalDataForm  implements Serializable {

	private static final long serialVersionUID = 2817998148091067992L;
	
	/*
	 * properties
	 */
	@NotNull
	private String firstName;
	@NotNull
	private String lastName;
	
	
	/*
	 * getters and setters
	 */
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
