package org.sklsft.connect.api.model.account.of;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.sklsft.connect.api.model.pattern.Patterns;

public class ResetPasswordForm implements Serializable {
	

	private static final long serialVersionUID = -6941944641662136995L;
	
	/*
	 * properties
	 */	
	@NotNull
	@Size(min=Patterns.MIN_PASSWORD_LENGTH, max=Patterns.MAX_PASSWORD_LENGTH, message="{password.length.invalid}")
	private String newPassword;
	
	private String token;

	
	/*
	 * getters and setters
	 */
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
