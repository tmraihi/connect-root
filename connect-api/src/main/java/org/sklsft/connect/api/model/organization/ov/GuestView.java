package org.sklsft.connect.api.model.organization.ov;

import java.io.Serializable;

import org.sklsft.connect.api.model.account.ov.AccountPublicBasicView;
import org.sklsft.connect.api.model.organization.of.MemberForm;

public class GuestView implements Serializable {

	private static final long serialVersionUID = -542945568072367244L;
	
	
	/*
	 * properties
	 */
	private String email;	
	private AccountPublicBasicView account = new AccountPublicBasicView();
	private MemberForm member = new MemberForm();
	
	
	/*
	 * getters and setters
	 */
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public AccountPublicBasicView getAccount() {
		return account;
	}
	public void setAccount(AccountPublicBasicView account) {
		this.account = account;
	}
	public MemberForm getMember() {
		return member;
	}
	public void setMember(MemberForm member) {
		this.member = member;
	}
}
