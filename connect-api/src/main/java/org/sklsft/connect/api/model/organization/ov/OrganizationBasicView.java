package org.sklsft.connect.api.model.organization.ov;

import java.io.Serializable;


public class OrganizationBasicView implements Serializable {
	
	private static final long serialVersionUID = -5394581468967421789L;
	
	/*
	 * properties
	 */
	private Long id;
	private String code;
	private String name;
	private String description;
	private String relativeUrl;
	
	
	/*
	 * getters and setters
	 */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRelativeUrl() {
		return relativeUrl;
	}
	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}
}
