package org.sklsft.connect.api.model.resources.of;

import java.io.Serializable;

public class AttachmentForm implements Serializable {
	
	private static final long serialVersionUID = -4569852084701345811L;
	
	
	public AttachmentForm(String attachmentName, byte[] data) {
		super();
		this.attachmentName = attachmentName;
		this.data = data;
	}
	
	public AttachmentForm() {

	}
	
	
	/*
	 * properties
	 */
	private String attachmentName;
	private byte[] data;
	
	
	/*
	 * getters and setters
	 */
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
}
