package org.sklsft.connect.api.model.organization.of;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.sklsft.connect.api.model.pattern.Patterns;

public class OrganizationCodeForm implements Serializable {

	private static final long serialVersionUID = -1198445305303562855L;

	/*
	 * properties
	 */
	@NotNull
	@Pattern(regexp = Patterns.ORGANIZATION_CODE_PATTERN , message = "{organizationCode.pattern.invalid}")
	private String code;

	
	/*
	 * getters and setters
	 */
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
