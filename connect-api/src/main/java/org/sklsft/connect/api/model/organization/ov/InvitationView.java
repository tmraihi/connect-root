package org.sklsft.connect.api.model.organization.ov;

import java.io.Serializable;

import org.sklsft.connect.api.model.organization.of.MemberForm;

public class InvitationView implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/*
	 * properties
	 */
	private OrganizationBasicView organization;
	private MemberForm member;
	
	
	/*
	 * getters and setters
	 */
	public OrganizationBasicView getOrganization() {
		return organization;
	}
	public void setOrganization(OrganizationBasicView organization) {
		this.organization = organization;
	}
	public MemberForm getMember() {
		return member;
	}
	public void setMember(MemberForm member) {
		this.member = member;
	}
}
