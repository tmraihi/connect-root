package org.sklsft.connect.api.interfaces.account;

import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.connect.api.model.account.of.AccountCreationForm;
import org.sklsft.connect.api.model.account.of.AccountPasswordForm;
import org.sklsft.connect.api.model.account.of.MailForm;
import org.sklsft.connect.api.model.account.of.PersonalDataForm;
import org.sklsft.connect.api.model.account.of.ResetPasswordForm;
import org.sklsft.connect.api.model.account.ov.AccountBasicView;
import org.sklsft.connect.api.model.account.ov.AccountFullView;
import org.sklsft.connect.api.model.authentication.ov.TokenView;

public interface AccountService {
	
	/**
	 * records an account and sends a mail to validate the given email address
	 * returns a token
	 */
	TokenView createAccount(AccountCreationForm form);
	static final String CREATE_ACCOUNT_URL = "/account";

	/**
	 * activates the mail given the token sent by mail (clickable link) returns
	 * a token
	 */
	TokenView activateEmail(String token);
	static final String ACTIVATE_EMAIL_URL = "/account/activate-email";

	/**
	 * ask for sending a new mail with a link to validate it
	 */
	void resendValidationMail();
	static final String RESEND_VALIDATION_EMAIL_URL = "/account/resend-email";

	/**
	 * get my account informations necessary for filling the header and the
	 * index page
	 */
	AccountBasicView retrieveMyAccount();
	static final String RETRIEVE_MY_ACCOUNT_URL = "/account";
	
	/**
	 * get my account informations for parameter page
	 */
	AccountFullView retrieveMyFullAccount();
	static final String RETRIEVE_MY_FULL_ACCOUNT_URL = "/account/full";

	/**
	 * updates data such as my first and last name
	 */
	void updateMyPersonalData(PersonalDataForm form);
	static final String UPDATE_MY_PERSONNAL_DATA_URL = "/account/personnal-data";

	/**
	 * updates my password
	 */
	void updateMyPassword(AccountPasswordForm accountPasswordForm);
	static final String UPDATE_MY_PASSWORD_URL = "/account/password";

	/**
	 * updates my email<br>
	 * requires a new validation of the email
	 */
	void updateMyMail(MailForm form);
	static final String UPDATE_MY_EMAIL_URL = "/account/email";


	/**
	 * upload a logo as a binary array that will be stored in a mongo gridfs
	 */
	void uploadMyAccountLogo(byte[] data);
	static final String UPLOAD_MY_LOGO_URL = "/account/logo";

	
	/**
	 * send an email to reset password for an account
	 * 
	 * @param emailForm
	 *            the email to search from
	 * @throws ObjectNotFoundException
	 */
	void sendResetPassword(MailForm emailForm);
	static final String SEND_RESET_PASSWORD_URL = "/account/reset-password";


	/**
	 * Reset a password and resend the new token
	 * 
	 * @param resetPasswordForm
	 */
	TokenView resetPassword(ResetPasswordForm resetPasswordForm);
	static final String RESET_PASSWORD_URL = "/account/reset-password";
}
