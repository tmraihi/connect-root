package org.sklsft.connect.api.model.organization.of;

import java.io.Serializable;

import javax.validation.constraints.NotNull;


public class OrganizationForm implements Serializable {

	private static final long serialVersionUID = -1198445305303562855L;

	/*
	 * properties
	 */
	@NotNull
	private String name;
	
	@NotNull
	private String description;

	
	/*
	 * getters and setters
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
