package org.sklsft.connect.api.model.organization.ov;

import java.io.Serializable;

import org.sklsft.connect.api.model.account.ov.AccountPublicBasicView;
import org.sklsft.connect.api.model.organization.of.MemberForm;

public class MemberView implements Serializable {

	private static final long serialVersionUID = -542945568072367244L;
	
	
	/*
	 * properties
	 */
	private AccountPublicBasicView account = new AccountPublicBasicView();
	private String role;
	private String position;
	private MemberForm memberForm = new MemberForm();
	
	
	/*
	 * getters and setters
	 */
	public AccountPublicBasicView getAccount() {
		return account;
	}
	public void setAccount(AccountPublicBasicView account) {
		this.account = account;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public MemberForm getMemberForm() {
		return memberForm;
	}
	public void setMemberForm(MemberForm memberForm) {
		this.memberForm = memberForm;
	}
}
