package org.sklsft.connect.api.model.account.ov;

import java.io.Serializable;


public class AccountPublicBasicView  implements Serializable {

	private static final long serialVersionUID = 2817998148091067992L;
	
	/*
	 * properties
	 */
	private Long id;
	private String firstName;
	private String lastName;
	private String relativeUrl;
	
	
	
	/*
	 * getters and setters
	 */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getRelativeUrl() {
		return relativeUrl;
	}
	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}
}
