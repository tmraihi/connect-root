package org.sklsft.connect.api.model.account.ov;

import java.io.Serializable;

import org.sklsft.connect.api.model.account.of.MailForm;
import org.sklsft.connect.api.model.account.of.PersonalDataForm;

public class AccountFullView implements Serializable {
	
	private static final long serialVersionUID = -3013704231731941149L;
	
	/*
	 * properties
	 */
	private PersonalDataForm personalData = new PersonalDataForm();
	private MailForm emailForm = new MailForm();
	private String relativeUrl;
	
	/*
	 * getters and setters
	 */
	public PersonalDataForm getPersonalData() {
		return personalData;
	}
	public void setPersonalData(PersonalDataForm accountBasicDataForm) {
		this.personalData = accountBasicDataForm;
	}
	public MailForm getEmailForm() {
		return emailForm;
	}
	public void setEmailForm(MailForm emailForm) {
		this.emailForm = emailForm;
	}
	public String getRelativeUrl() {
		return relativeUrl;
	}
	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}
}
