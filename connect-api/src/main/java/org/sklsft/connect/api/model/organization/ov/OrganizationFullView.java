package org.sklsft.connect.api.model.organization.ov;

import java.io.Serializable;

import org.sklsft.connect.api.model.organization.of.OrganizationCodeForm;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;

public class OrganizationFullView implements Serializable {

	private static final long serialVersionUID = -4276119236203115680L;

	/*
	 * properties
	 */
	private Long id;
	private OrganizationCodeForm organizationCodeForm = new OrganizationCodeForm();
	private OrganizationForm organizationForm = new OrganizationForm();
	private String relativeUrl;
	
	
	/*
	 * getters and setters
	 */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public OrganizationCodeForm getOrganizationCodeForm() {
		return organizationCodeForm;
	}
	public void setOrganizationCodeForm(OrganizationCodeForm organizationCodeForm) {
		this.organizationCodeForm = organizationCodeForm;
	}
	public OrganizationForm getOrganizationForm() {
		return organizationForm;
	}
	public void setOrganizationForm(OrganizationForm organizationForm) {
		this.organizationForm = organizationForm;
	}
	public String getRelativeUrl() {
		return relativeUrl;
	}
	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}
}
