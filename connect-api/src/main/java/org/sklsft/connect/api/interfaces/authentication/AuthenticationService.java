package org.sklsft.connect.api.interfaces.authentication;

import org.sklsft.connect.api.model.authentication.of.LoginForm;
import org.sklsft.connect.api.model.authentication.ov.TokenView;

public interface AuthenticationService {

	/**
	 * provides an access token
	 */
	TokenView authenticate(LoginForm form);
	static final String AUTHENTICATE_URL = "/authenticate";
	
}
