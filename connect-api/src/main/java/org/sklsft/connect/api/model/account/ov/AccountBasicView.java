package org.sklsft.connect.api.model.account.ov;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.sklsft.connect.api.model.organization.ov.OrganizationBasicView;

public class AccountBasicView implements Serializable {
	
	private static final long serialVersionUID = -3013704231731941149L;
	
	/*
	 * properties
	 */
	private String firstName;
	private String lastName;
	private Boolean emailValidated;
	private String relativeUrl;
	private Long userId;

	List<OrganizationBasicView> attachedOrganizations = new ArrayList<>();
	
	
	/*
	 * getters and setters
	 */
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Boolean getEmailValidated() {
		return emailValidated;
	}	
	public void setEmailValidated(Boolean emailValidated) {
		this.emailValidated = emailValidated;
	}
	public List<OrganizationBasicView> getAttachedOrganizations() {
		return attachedOrganizations;
	}
	public void setAttachedOrganizations(
			List<OrganizationBasicView> attachedOrganizations) {
		this.attachedOrganizations = attachedOrganizations;
	}
	public String getRelativeUrl() {
		return relativeUrl;
	}
	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
