package org.sklsft.connect.api.model.resources.ov;

import java.io.Serializable;

public class AttachmentView implements Serializable {

	private static final long serialVersionUID = -8993113899289656167L;
	
	
	public AttachmentView(Long id, String attachmentName, String relativeUrl) {
		super();
		this.id = id;
		this.attachmentName = attachmentName;
		this.relativeUrl = relativeUrl;
	}
	
	public AttachmentView() {
		
	}
	

	/*
	 * properties
	 */
	private Long id;
	private String attachmentName;
	private String relativeUrl;
	
	/*
	 * getters and setters
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttachmentName() {
		return attachmentName;
	}	
	
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public String getRelativeUrl() {
		return relativeUrl;
	}
	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}
}
