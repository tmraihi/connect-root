package org.sklsft.connect.api.interfaces.organization;

import java.util.List;

import org.sklsft.connect.api.model.organization.of.GuestForm;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.connect.api.model.organization.ov.GuestView;
import org.sklsft.connect.api.model.organization.ov.MemberView;

public interface OrganizationMembershipService {
		
	List<MemberView> getMyOrganizationMembers(Long id);
	static final String GET_MY_ORGANIZATION_MEMBERS_URL = "/organization/{id}/members";
	
	void updateMember(Long organizationId, Long accountId, MemberForm form);
	static final String UPDATE_MY_ORGANIZATION_MEMBER_URL = "/organization/{organizationId}/member/{accountId}";
	
	void deleteMember(Long organizationId, Long accountId);
	static final String DELETE_MY_ORGANIZATION_MEMBER_URL = "/organization/{organizationId}/member/{accountId}";
	
	List<GuestView> getMyOrganizationGuests(Long id);
	static final String GET_MY_ORGANIZATION_GUESTS_URL = "/organization/{id}/guests";
	
	void inviteMember(Long id, GuestForm form);
	static final String INVITE_MEMBER_URL = "/organization/{id}/guest";
	
	void cancelInviteMember(Long id, String eMail);
	static final String CANCEL_INVITE_MEMBER_URL = "/organization/{id}/guest/{email}";
	
	void acceptJoinOrganization(Long id);
	static final String ACCEPT_INVITATION_URL = "/organization/{id}/join";
	
	void refuseJoinOrganization(Long id);
	static final String REFUSE_INVITATION_URL = "/organization/{id}/decline";
	
	void leaveOrganization(Long id);
	static final String LEAVE_ORGANIZATION_URL = "/organization/{id}/leave";
}
