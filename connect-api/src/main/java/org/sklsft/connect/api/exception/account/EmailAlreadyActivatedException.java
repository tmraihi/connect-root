package org.sklsft.connect.api.exception.account;

import org.sklsft.commons.api.exception.ApplicationException;


/**
 * Exception thrown when trying to validate a mail already validated
 */
public class EmailAlreadyActivatedException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	
	public EmailAlreadyActivatedException(){
		super();
	}
	
	public EmailAlreadyActivatedException(String message) {
		super(message);

	}

	public EmailAlreadyActivatedException(String message, Throwable cause) {
		super(message, cause);
	}

}
