package org.sklsft.connect.api.interfaces.organization;

import java.util.List;

import org.sklsft.connect.api.model.organization.of.OrganizationCodeForm;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.sklsft.connect.api.model.organization.ov.InvitationView;
import org.sklsft.connect.api.model.organization.ov.OrganizationBasicView;
import org.sklsft.connect.api.model.organization.ov.OrganizationFullView;

public interface OrganizationService {

	/**
	 * a private access to all my organizations
	 */
	List<OrganizationBasicView> retrieveMyOrganizations();
	static final String GET_MY_ORGANIZATION_LIST_URL = "/organization/list";
	
	
	/**
	 * a private access to all my invitations
	 */
	List<InvitationView> retrieveMyInvitations();
	static final String GET_MY_INVITATION_LIST_URL = "/invitation/list";
	
	
	/**
	 * a private access to any of my organizations
	 */
	OrganizationFullView retrieveMyOrganization(Long id);
	static final String GET_MY_ORGANIZATION_URL = "/organization/{id}";
	
	
	/**
	 * Check if an organization code already exists. A conversion will be
	 * processed if necessary. we return false if same code is passed while retrieving the organization
	 * @param code raw code form to check
	 * @return true if already exists, false otherwise
	 */
	boolean existsOrganization(String code);
	static final String EXISTS_ORGANIZATION_URL = "/organization/{code}/exists";
	

	/**
	 * create a new organization of which I am the owner.
	 */
	Long createOrganization(OrganizationForm form);
	static final String CREATE_ORGANIZATION_URL = "/organization";
	

	/**
	 * update one of my organizations
	 */
	void updateMyOrganization(Long id, OrganizationForm form);
	static final String UPDATE_MY_ORGANIZATION_URL = "/organization/{id}";
	
	
	/**
	 * delete organization
	 */
	void deleteMyOrganization(Long id);
	static final String DELETE_MY_ORGANIZATION_URL = "/organization/{id}";
	

	/**
	 * updates the identifier of one of my organizations
	 */
	void updateMyOrganizationIdentifier(Long id, OrganizationCodeForm organizationCodeForm);
	static final String UPDATE_MY_ORGANIZATION_IDENTIFIER_URL = "/organization/{id}/identifier";

	
	/**
	 * upload the logo of one of my organizations
	 */
	void uploadMyOrganizationLogo(Long id, byte[] bytes);
	static final String UPLOAD_MY_ORGANIZATION_LOGO_URL = "/organization/{id}/logo";
	
}
