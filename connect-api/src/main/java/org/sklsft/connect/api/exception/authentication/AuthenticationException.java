package org.sklsft.connect.api.exception.authentication;

import org.sklsft.commons.api.exception.ApplicationException;


/**
 * Exception thrown afer an authentication failure
 */
public class AuthenticationException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	
	public AuthenticationException(){
		super();
	}
	
	public AuthenticationException(String message) {
		super(message);

	}

	public AuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}
}
