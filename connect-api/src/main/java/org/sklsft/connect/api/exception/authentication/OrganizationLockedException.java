package org.sklsft.connect.api.exception.authentication;

import org.sklsft.commons.api.exception.ApplicationException;


/**
 * Exception thrown when trying to give a token to a desactivated account
 *
 */
public class OrganizationLockedException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;

	
	public OrganizationLockedException() {
		super();

	}
	
	public OrganizationLockedException(String message) {
		super(message);

	}

	public OrganizationLockedException(String message, Throwable cause) {
		super(message, cause);
	}
}
