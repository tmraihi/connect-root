package org.sklsft.connect.rest.controller.account;



import javax.inject.Inject;
import javax.validation.Valid;

import org.sklsft.commons.rest.security.access.AccessControlType;
import org.sklsft.commons.rest.security.annotations.AccessControl;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.account.of.AccountCreationForm;
import org.sklsft.connect.api.model.account.of.AccountPasswordForm;
import org.sklsft.connect.api.model.account.of.MailForm;
import org.sklsft.connect.api.model.account.of.PersonalDataForm;
import org.sklsft.connect.api.model.account.of.ResetPasswordForm;
import org.sklsft.connect.api.model.account.ov.AccountBasicView;
import org.sklsft.connect.api.model.account.ov.AccountFullView;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
public class AccountController {

	
	@Inject
	private AccountService accountService;

	@AccessControl(AccessControlType.ANONYMOUS)
	@RequestMapping(value = AccountService.CREATE_ACCOUNT_URL, method=RequestMethod.POST)
	public @ResponseBody TokenView createAccount(@RequestBody @Valid AccountCreationForm form){
		return accountService.createAccount(form);
	}
	
	
	@RequestMapping(value = AccountService.UPDATE_MY_PERSONNAL_DATA_URL, method=RequestMethod.PUT)
	public @ResponseBody void updateMyPersonalData(@RequestBody @Valid PersonalDataForm form){
		accountService.updateMyPersonalData(form);
	}
	
	
	@AccessControl(AccessControlType.ANONYMOUS)
	@RequestMapping(value = AccountService.ACTIVATE_EMAIL_URL, method=RequestMethod.POST)
	public @ResponseBody TokenView activateEmail(@RequestBody String token){
		return accountService.activateEmail(token);
	}
	
	
	@RequestMapping(value = AccountService.RESEND_VALIDATION_EMAIL_URL, method=RequestMethod.GET)
	public @ResponseBody void resendValidationMail (){
		accountService.resendValidationMail();
	}
	
	
	@RequestMapping(value = AccountService.UPDATE_MY_EMAIL_URL, method=RequestMethod.PUT)
	public @ResponseBody void updateMyMail (@RequestBody @Valid MailForm form){
		accountService.updateMyMail(form);
	}
	
	
	@RequestMapping(value = AccountService.UPDATE_MY_PASSWORD_URL, method=RequestMethod.PUT)
	public @ResponseBody void updatePassword(@RequestBody @Valid AccountPasswordForm accountPasswordForm){
		accountService.updateMyPassword(accountPasswordForm);
	}
	
	
	@AccessControl(AccessControlType.ANONYMOUS)
	@RequestMapping(value = AccountService.SEND_RESET_PASSWORD_URL, method = RequestMethod.PUT)
	public @ResponseBody
	void sendResetPassword(@RequestBody @Valid MailForm emailForm) {
		// send email
		accountService.sendResetPassword(emailForm);
	}


	@AccessControl(AccessControlType.ANONYMOUS)
	@RequestMapping(value = AccountService.RESET_PASSWORD_URL, method = RequestMethod.POST)
	public @ResponseBody
	TokenView resetPassword(@RequestBody @Valid ResetPasswordForm resetPasswordForm) {
		return accountService.resetPassword(resetPasswordForm);
	}
	
	
	@RequestMapping(value = AccountService.RETRIEVE_MY_ACCOUNT_URL, method=RequestMethod.GET)
	public @ResponseBody AccountBasicView retriveMyAccount(){
		return accountService.retrieveMyAccount();
	}
	
	
	@RequestMapping(value = AccountService.RETRIEVE_MY_FULL_ACCOUNT_URL, method=RequestMethod.GET)
	public @ResponseBody AccountFullView retriveMyFullAccount(){
		return accountService.retrieveMyFullAccount();
	}
	
	
	@RequestMapping(value = AccountService.UPLOAD_MY_LOGO_URL, method =RequestMethod.PUT)
	public @ResponseBody void uploadMyAccountLogo (@RequestBody byte[] data) {
		accountService.uploadMyAccountLogo(data);
	  
	}
}
