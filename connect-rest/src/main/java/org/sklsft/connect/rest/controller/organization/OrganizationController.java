package org.sklsft.connect.rest.controller.organization;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.sklsft.connect.api.interfaces.organization.OrganizationService;
import org.sklsft.connect.api.model.organization.of.OrganizationCodeForm;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.sklsft.connect.api.model.organization.ov.InvitationView;
import org.sklsft.connect.api.model.organization.ov.OrganizationBasicView;
import org.sklsft.connect.api.model.organization.ov.OrganizationFullView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class OrganizationController {

	@Inject
	private OrganizationService organizationService;
	

	@RequestMapping(value = OrganizationService.GET_MY_ORGANIZATION_LIST_URL  , method = RequestMethod.GET)
	public @ResponseBody List<OrganizationBasicView> retrieveMyOrganizations() {
		return organizationService.retrieveMyOrganizations();
	}
	
	
	@RequestMapping(value = OrganizationService.GET_MY_INVITATION_LIST_URL  , method = RequestMethod.GET)
	public @ResponseBody List<InvitationView> retrieveMyInvitations() {
		return organizationService.retrieveMyInvitations();
	}
	
	
	@RequestMapping(value = OrganizationService.GET_MY_ORGANIZATION_URL  , method = RequestMethod.GET)
	public @ResponseBody OrganizationFullView retrieveMyOrganization(@PathVariable("id") Long id) {
		return organizationService.retrieveMyOrganization(id);
	}
	
	
	@RequestMapping(value = OrganizationService.EXISTS_ORGANIZATION_URL , method = RequestMethod.GET)
	public @ResponseBody boolean checkCodeAlreadyExists(@PathVariable("code") String code) {
		return organizationService.existsOrganization(code);
	}
	

	@RequestMapping(value = OrganizationService.CREATE_ORGANIZATION_URL , method = RequestMethod.POST)
	public @ResponseBody Long create(@RequestBody @Valid OrganizationForm form) {
		return organizationService.createOrganization(form);
	}
	

	@RequestMapping(value = OrganizationService.UPDATE_MY_ORGANIZATION_URL , method = RequestMethod.PUT)
	public @ResponseBody void update(@PathVariable("id") Long id, @RequestBody @Valid OrganizationForm form) {
		organizationService.updateMyOrganization(id, form);
	}
	
	
	@RequestMapping(value = OrganizationService.DELETE_MY_ORGANIZATION_URL , method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id") Long id) {
		organizationService.deleteMyOrganization(id);
	}


	@RequestMapping(value = OrganizationService.UPDATE_MY_ORGANIZATION_IDENTIFIER_URL , method = RequestMethod.PUT)
	public @ResponseBody void updateMyOrganizationIdentifier(@PathVariable("id") Long id, @RequestBody @Valid OrganizationCodeForm organizationCodeForm) {
		organizationService.updateMyOrganizationIdentifier(id, organizationCodeForm);
	}


	@RequestMapping(value = OrganizationService.UPLOAD_MY_ORGANIZATION_LOGO_URL, method = RequestMethod.PUT)
	public @ResponseBody void uploadOrganizationLogo(@PathVariable("id") Long id, @RequestBody byte[] data) {
		organizationService.uploadMyOrganizationLogo(id, data);
	}
}
