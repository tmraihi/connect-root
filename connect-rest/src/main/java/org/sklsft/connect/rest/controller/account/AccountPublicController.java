package org.sklsft.connect.rest.controller.account;

import javax.inject.Inject;

import org.sklsft.commons.rest.security.access.AccessControlType;
import org.sklsft.commons.rest.security.annotations.AccessControl;
import org.sklsft.commons.rest.security.tokens.extraction.TokenExtractionMode;
import org.sklsft.connect.api.interfaces.account.AccountPublicService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = "/account/public")
public class AccountPublicController {

	@Inject
	private AccountPublicService accountPublicService;

	
	@AccessControl(value=AccessControlType.PRIVATE, tokenExtractionMode=TokenExtractionMode.COOKIE)
	@RequestMapping(value = "/{id}/logo", method=RequestMethod.GET, produces = "image/jpg")
	public @ResponseBody byte[] getAccountLogo(@PathVariable("id") Long id){
		return accountPublicService.getAccountLogo(id);
	}
}
