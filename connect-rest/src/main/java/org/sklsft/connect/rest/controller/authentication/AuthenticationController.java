package org.sklsft.connect.rest.controller.authentication;

import javax.inject.Inject;
import javax.validation.Valid;

import org.sklsft.commons.rest.security.access.AccessControlType;
import org.sklsft.commons.rest.security.annotations.AccessControl;
import org.sklsft.connect.api.interfaces.authentication.AuthenticationService;
import org.sklsft.connect.api.model.authentication.of.LoginForm;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class AuthenticationController {

	@Inject
	private AuthenticationService authenticationService;
	
	@AccessControl(AccessControlType.ANONYMOUS)
	@RequestMapping(value = AuthenticationService.AUTHENTICATE_URL, method=RequestMethod.POST)
	public @ResponseBody TokenView login(@RequestBody @Valid LoginForm form){
		return authenticationService.authenticate(form);
	}
}
