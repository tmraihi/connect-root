package org.sklsft.connect.rest.controller.organization;

import javax.inject.Inject;

import org.sklsft.commons.rest.security.access.AccessControlType;
import org.sklsft.commons.rest.security.annotations.AccessControl;
import org.sklsft.connect.api.interfaces.organization.OrganizationPublicService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class OrganizationPublicController {

	@Inject
	private OrganizationPublicService organizationPublicService;
	

	@AccessControl(AccessControlType.PUBLIC)
	@RequestMapping(value = OrganizationPublicService.GET_ORGANIZATION_LOGO_URL , method = RequestMethod.GET, produces = "image/jpg")
	public @ResponseBody byte[] getOrganizationLogo(@PathVariable("id") Long id) {
		return organizationPublicService.getOrganizationLogo(id);
	}
}