package org.sklsft.connect.rest.controller.organization;

import java.util.List;

import javax.inject.Inject;

import org.sklsft.connect.api.interfaces.organization.OrganizationMembershipService;
import org.sklsft.connect.api.model.organization.of.GuestForm;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.connect.api.model.organization.ov.GuestView;
import org.sklsft.connect.api.model.organization.ov.MemberView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class OrgnaizationMembershipController {
	
	@Inject
	private OrganizationMembershipService organizationMembershipService;
	
	
	@RequestMapping(value = OrganizationMembershipService.GET_MY_ORGANIZATION_MEMBERS_URL , method = RequestMethod.GET)
	public @ResponseBody List<MemberView> getMyOrganizationMembers(@PathVariable("id") Long id) {
		return organizationMembershipService.getMyOrganizationMembers(id);
	}	
	
	
	@RequestMapping(value = OrganizationMembershipService.UPDATE_MY_ORGANIZATION_MEMBER_URL, method = RequestMethod.PUT)
	public @ResponseBody void updateMemeber(@PathVariable("organizationId") Long organizationId, @PathVariable("accountId") Long accountId, @RequestBody MemberForm form) {
		organizationMembershipService.updateMember(organizationId, accountId, form);

	}
	
	
	@RequestMapping(value = OrganizationMembershipService.DELETE_MY_ORGANIZATION_MEMBER_URL, method = RequestMethod.DELETE)
	public @ResponseBody void deleteMember(@PathVariable("organizationId") Long organizationId, @PathVariable("accountId") Long accountId) {
		organizationMembershipService.deleteMember(organizationId, accountId);

	}
	
	
	@RequestMapping(value = OrganizationMembershipService.GET_MY_ORGANIZATION_GUESTS_URL , method = RequestMethod.GET)
	public @ResponseBody List<GuestView> getMyOrganizationGuests(@PathVariable("id") Long id) {
		return organizationMembershipService.getMyOrganizationGuests(id);
	}
	
	
	@RequestMapping(value = OrganizationMembershipService.INVITE_MEMBER_URL, method = RequestMethod.PUT)
	public @ResponseBody void inviteMember(@PathVariable("id") Long id, @RequestBody GuestForm form) {
		organizationMembershipService.inviteMember(id, form);
	}
	
	
	@RequestMapping(value = OrganizationMembershipService.CANCEL_INVITE_MEMBER_URL, method = RequestMethod.DELETE)
	public @ResponseBody void cancelInviteMember(@PathVariable("id") Long id, String email) {
		organizationMembershipService.cancelInviteMember(id, email);
	}
	
	
	@RequestMapping(value = OrganizationMembershipService.ACCEPT_INVITATION_URL, method = RequestMethod.PUT)
	public @ResponseBody
	void acceptJoinOrganization(@PathVariable("id") Long id) {
		organizationMembershipService.acceptJoinOrganization(id);
	}
	
	
	@RequestMapping(value = OrganizationMembershipService.REFUSE_INVITATION_URL, method = RequestMethod.PUT)
	public @ResponseBody void refuseJoinOrganization(@PathVariable("id") Long id) {
		organizationMembershipService.refuseJoinOrganization(id);
	}
	
	
	@RequestMapping(value = OrganizationMembershipService.LEAVE_ORGANIZATION_URL, method = RequestMethod.PUT)
	public @ResponseBody void leaveOrganization(@PathVariable("id") Long id) {
		organizationMembershipService.leaveOrganization(id);
	}
}
