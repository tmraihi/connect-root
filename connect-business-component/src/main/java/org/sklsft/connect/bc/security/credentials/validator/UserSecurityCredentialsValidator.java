package org.sklsft.connect.bc.security.credentials.validator;

import java.util.Date;

import javax.inject.Inject;

import org.apache.commons.lang.time.DateUtils;
import org.sklsft.commons.rest.security.credentials.validator.SecurityCredentialsValidator;
import org.sklsft.connect.api.exception.authentication.InvalidTokenException;
import org.sklsft.connect.api.exception.authentication.TokenExpiredException;
import org.sklsft.connect.bc.security.credentials.UserSecurityCredentials;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("userSecurityCredentialsValidator")
public class UserSecurityCredentialsValidator implements SecurityCredentialsValidator<UserSecurityCredentials> {
	
	private static final int expiryDelayInDays = 3;
	
	@Inject
	private UserAccountDao userAccountDao;
	
	@Transactional(readOnly=true)
	public void validateCredentials(UserSecurityCredentials credentials) {
		
		UserAccount userAccount;
		
		try {
			userAccount = userAccountDao.load(credentials.getAccountId());
		} catch (Exception e) {
			throw new InvalidTokenException("token.invalid", e);
		}
		
		if (!(userAccount.getCreationDate().getTime() == credentials.getAccountCreationDate().getTime())) {
			throw new InvalidTokenException("token.invalid");
		}
		
		Date expiryDate = DateUtils.addDays(credentials.getCreationDate(), expiryDelayInDays);
		if (expiryDate.before(new Date())) {
			throw new TokenExpiredException("token.expired");
		}
	}

}
