package org.sklsft.connect.bc.mapper.organization;

import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.commons.mapper.interfaces.Mapper;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.sklsft.social.model.organization.Organization;
import org.springframework.stereotype.Component;

@Component
public class OrganizationFormMapper extends BasicMapperImpl<OrganizationForm, Organization> implements Mapper<OrganizationForm, Organization>{
	
	public OrganizationFormMapper() {
		super(OrganizationForm.class, Organization.class);
	}
	
	
	@Override
	public OrganizationForm mapFrom(OrganizationForm form, Organization organization) {

		form = super.mapFrom(form, organization);
		
		return form;
	}
	
	@Override
	public Organization mapTo(OrganizationForm form, Organization organization) {
		
		organization = super.mapTo(form, organization);
		
		return organization;
	}
	
}
