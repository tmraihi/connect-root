package org.sklsft.connect.bc.mapper.organization;

import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.organization.of.GuestForm;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.util.roles.OrganizationRole;
import org.springframework.stereotype.Component;

@Component
public class GuestFormMapper extends BasicMapperImpl<GuestForm, OrganizationGuest> {

	public GuestFormMapper() {
		super(GuestForm.class, OrganizationGuest.class);
	}
	
	
	@Override
	public GuestForm mapFrom(GuestForm form, OrganizationGuest organizationGuest) {
		
		form = super.mapFrom(form, organizationGuest);
		form.getMemberForm().setPosition(organizationGuest.getPosition());
		form.getMemberForm().setRole(OrganizationRole.valueOf(organizationGuest.getRole()));
		form.getMailForm().setEmail(organizationGuest.getEmail());
		
		return form;
	}
	
	
	@Override
	public OrganizationGuest mapTo(GuestForm form, OrganizationGuest organizationGuest) {
		
		organizationGuest = super.mapTo(form, organizationGuest);
		organizationGuest.setRole(form.getMemberForm().getRole().name());
		organizationGuest.setPosition(form.getMemberForm().getPosition());
		
		return organizationGuest;
	}
}
