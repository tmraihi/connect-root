package org.sklsft.connect.bc.mapper.organization;

import javax.inject.Inject;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.connect.api.model.organization.ov.MemberView;
import org.sklsft.connect.bc.mapper.account.AccountPublicBasicViewMapper;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.util.roles.OrganizationRole;
import org.springframework.stereotype.Component;


@Component
public class MemberViewMapper extends BasicMapperImpl<MemberView, OrganizationMember> {

	public MemberViewMapper() {
		super(MemberView.class, OrganizationMember.class);
	}
	
	@Inject
	private AccountPublicBasicViewMapper accountPublicBasicViewMapper;
	
	@Inject
	private MemberFormMapper memberFormMapper;
	
		
	
	@Override
	public MemberView mapFrom(MemberView view, OrganizationMember organizationMember) {
		
		view = super.mapFrom(view, organizationMember);
		view.setAccount(accountPublicBasicViewMapper.mapFrom(organizationMember.getUserAccount()));
		view.setRole(OrganizationRole.valueOf(organizationMember.getRole()).getLabel());
		view.setMemberForm(memberFormMapper.mapFrom(new MemberForm(), organizationMember));
		
		return view;
	}
	
	
	@Override
	public OrganizationMember mapTo(MemberView view, OrganizationMember organizationMember) {
		
		throw new NotImplementedException();
	}
}
