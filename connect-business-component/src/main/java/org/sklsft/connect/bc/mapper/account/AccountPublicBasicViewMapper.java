package org.sklsft.connect.bc.mapper.account;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.account.ov.AccountPublicBasicView;
import org.sklsft.social.model.account.UserAccount;
import org.springframework.stereotype.Component;

@Component
public class AccountPublicBasicViewMapper extends BasicMapperImpl<AccountPublicBasicView, UserAccount> {
		
	public AccountPublicBasicViewMapper() {
		super(AccountPublicBasicView.class, UserAccount.class);
	}

	@Override
	public AccountPublicBasicView mapFrom(AccountPublicBasicView view, UserAccount userAccount) {
		
		view = super.mapFrom(view, userAccount);
		view.setRelativeUrl(userAccount.getRelativeUrl());
		
		return view;
	}
	
	
	@Override
	public UserAccount mapTo(AccountPublicBasicView view, UserAccount userAccount) {
		
		throw new NotImplementedException();
	}

}
