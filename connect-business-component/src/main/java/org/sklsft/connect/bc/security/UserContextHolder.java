package org.sklsft.connect.bc.security;

import javax.inject.Inject;

import org.sklsft.commons.rest.security.context.SecurityContextHolder;
import org.sklsft.connect.bc.security.credentials.UserSecurityCredentials;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;
import org.springframework.stereotype.Component;

@Component
public class UserContextHolder {
	
	@Inject
	private OrganizationDao organizationDao;
	
	@Inject
	private UserAccountDao userAccountDao;

	

	public UserAccount getCurrentAccount() {
		Long accountId = ((UserSecurityCredentials)SecurityContextHolder.getCredentials())
				.getAccountId();
		return userAccountDao.load(accountId);
	}
}
