package org.sklsft.connect.bc.mapper.account;

import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.account.of.PersonalDataForm;
import org.sklsft.social.model.account.UserAccount;
import org.springframework.stereotype.Component;

@Component
public class PersonalDataFormMapper extends BasicMapperImpl<PersonalDataForm, UserAccount> {
		
	public PersonalDataFormMapper() {
		super(PersonalDataForm.class, UserAccount.class);
	}
	
	

	@Override
	public PersonalDataForm mapFrom(PersonalDataForm accountBasicDataForm, UserAccount userAccount) {
		
		accountBasicDataForm = super.mapFrom(accountBasicDataForm, userAccount);
		
		return accountBasicDataForm;
	}
	
	
	@Override
	public UserAccount mapTo(PersonalDataForm accountBasicDataForm, UserAccount userAccount) {
		
		userAccount = super.mapTo(accountBasicDataForm, userAccount);
		
		return userAccount;
	}

}
