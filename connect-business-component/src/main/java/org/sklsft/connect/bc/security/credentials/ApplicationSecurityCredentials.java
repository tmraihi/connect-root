package org.sklsft.connect.bc.security.credentials;

import java.io.Serializable;

public class ApplicationSecurityCredentials implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/*
	 * properties
	 */
	private Long applicationId;

	
	/*
	 * getters and setters
	 */
	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
}
