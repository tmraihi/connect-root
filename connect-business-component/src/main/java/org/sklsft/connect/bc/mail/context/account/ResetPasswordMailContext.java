package org.sklsft.connect.bc.mail.context.account;

import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.util.mail.context.MailContext;

/**
 * an url must be delivered with this mail to change password
 * {@see MailContext}
 * @author Nicolas Thibault
 *
 */
public class ResetPasswordMailContext extends MailContext {
	
	/*
	 * properties
	 */
	private String resetPasswordUrl;

	
	/*
	 * getters and setters
	 */
	public String getResetPasswordUrl() {
		return resetPasswordUrl;
	}

	public void setResetPasswordUrl(String resetPasswordUrl) {
		this.resetPasswordUrl = resetPasswordUrl;
	}

	
	/*
	 * constructor
	 */
	public ResetPasswordMailContext(UserAccount userAccount, String subject,
			String resetPasswordUrl) {
		super(userAccount.getEmail(), subject);
		this.resetPasswordUrl = resetPasswordUrl;
	}

}
