package org.sklsft.connect.bc.mail.manager.account;

import javax.inject.Inject;
import javax.mail.MessagingException;

import org.sklsft.connect.bc.mail.context.account.ResetPasswordMailContext;
import org.sklsft.connect.bc.mail.template.account.ResetPasswordMailPreparator;
import org.sklsft.connect.bc.security.token.TokenFactory;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.util.mail.model.UserAccountMail;
import org.sklsft.social.util.mail.model.UserAccountMailFactory;
import org.sklsft.social.util.mail.sender.interfaces.UserAccountMailSender;

public class ResetPasswordMailManager {

	/*
	 * this url has to be injected via a jndi variable in xml
	 */
	private String webServerUrl;
	
	public void setWebServerUrl(String webServerUrl) {
		this.webServerUrl = webServerUrl;
	}

	@Inject
	private TokenFactory tokenFactory;

	@Inject
	private UserAccountMailFactory userAccountMailFactory;
	
	@Inject
	private ResetPasswordMailPreparator mailPreparator;

	@Inject
	private UserAccountMailSender mailSender;
	

	public void sendResetPasswordMail(UserAccount userAccount)
			throws MessagingException {

		String subject = "Renouvellement de votre mot de passe.";

		String token = tokenFactory.buildToken(userAccount);
		String resetPasswordUrl = webServerUrl
				+ "/public/reset-password.jsf?token=" + token;

		ResetPasswordMailContext mailContext = new ResetPasswordMailContext(
				userAccount, subject, resetPasswordUrl);

		UserAccountMail mail = mailPreparator.prepareMail(mailContext, userAccountMailFactory);

		mailSender.sendMail(mail);

	}
}
