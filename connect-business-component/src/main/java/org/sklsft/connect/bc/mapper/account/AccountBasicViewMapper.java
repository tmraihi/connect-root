package org.sklsft.connect.bc.mapper.account;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.account.ov.AccountBasicView;
import org.sklsft.connect.api.model.organization.ov.OrganizationBasicView;
import org.sklsft.connect.bc.mapper.organization.OrganizationBasicViewMapper;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.organization.OrganizationMember;
import org.springframework.stereotype.Component;

@Component
public class AccountBasicViewMapper extends BasicMapperImpl<AccountBasicView, UserAccount> {
	
	public AccountBasicViewMapper() {
		super(AccountBasicView.class, UserAccount.class);
	}

	
	@Inject
	private OrganizationBasicViewMapper organizationBasicViewMapper;
	
	
	
	@Override
	public AccountBasicView mapFrom(AccountBasicView view, UserAccount userAccount) {
		
		view = super.mapFrom(view, userAccount);
		
		view.setRelativeUrl(userAccount.getRelativeUrl());	
		
		List<OrganizationBasicView> attachedOrganizations = new ArrayList<>();
		
		for (OrganizationMember organizationMember:userAccount.getOrganizationMemberCollection()) {			
			attachedOrganizations.add(organizationBasicViewMapper.mapFrom(organizationMember.getOrganization()));
		}
		
		view.setAttachedOrganizations(attachedOrganizations);
		
		return view;
	}
	
	@Override
	public UserAccount mapTo(AccountBasicView view, UserAccount userAccount) {
		
		throw new NotImplementedException();
	}

}
