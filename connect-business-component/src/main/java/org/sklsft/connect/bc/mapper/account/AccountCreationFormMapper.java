package org.sklsft.connect.bc.mapper.account;

import javax.inject.Inject;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.account.of.AccountCreationForm;
import org.sklsft.social.model.account.UserAccount;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AccountCreationFormMapper extends BasicMapperImpl<AccountCreationForm, UserAccount> {
	
	@Inject
	private PersonalDataFormMapper accountBasicDataFormMapper;
	
	@Inject
	private PasswordEncoder passwordEncoder;


	public AccountCreationFormMapper() {
		super(AccountCreationForm.class, UserAccount.class);
	}

	@Override
	public AccountCreationForm mapFrom(AccountCreationForm form, UserAccount userAccount) {
		
		throw new NotImplementedException();
	}
	
	@Override
	public UserAccount mapTo(AccountCreationForm form, UserAccount userAccount) {
		
		userAccount = super.mapTo(form, userAccount);
		userAccount = accountBasicDataFormMapper.mapTo(form.getPersonalData(), userAccount);
		userAccount.setPasswordHash(passwordEncoder.encode(form.getPassword()));
		userAccount.setUserRole(UserRoleEnum.USER_CANDIDATE_ROLE.toString());
		return userAccount;
	}

}
