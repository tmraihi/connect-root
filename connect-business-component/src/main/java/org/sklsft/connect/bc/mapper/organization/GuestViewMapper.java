package org.sklsft.connect.bc.mapper.organization;

import javax.inject.Inject;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.connect.api.model.organization.ov.GuestView;
import org.sklsft.connect.bc.mapper.account.AccountPublicBasicViewMapper;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.springframework.stereotype.Component;


@Component
public class GuestViewMapper extends BasicMapperImpl<GuestView, OrganizationGuest> {

	public GuestViewMapper() {
		super(GuestView.class, OrganizationGuest.class);
	}
	
	@Inject
	private AccountPublicBasicViewMapper accountPublicBasicViewMapper;
	
	@Inject
	private MemberFormFromGuestMapper memberFormMapper;
	
		
	
	@Override
	public GuestView mapFrom(GuestView view, OrganizationGuest guest) {
		
		view = super.mapFrom(view, guest);
		if (guest.getUserAccount()!=null) {
			view.setAccount(accountPublicBasicViewMapper.mapFrom(guest.getUserAccount()));
		}
		view.setMember(memberFormMapper.mapFrom(new MemberForm(), guest));
		
		return view;
	}
	
	
	@Override
	public OrganizationGuest mapTo(GuestView view, OrganizationGuest organizationMember) {
		
		throw new NotImplementedException();
	}
}
