package org.sklsft.connect.bc.mapper.organization;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.commons.mapper.interfaces.Mapper;
import org.sklsft.connect.api.model.organization.ov.OrganizationBasicView;
import org.sklsft.social.model.organization.Organization;
import org.springframework.stereotype.Component;

@Component
public class OrganizationBasicViewMapper extends BasicMapperImpl<OrganizationBasicView, Organization> implements Mapper<OrganizationBasicView, Organization>{

	public OrganizationBasicViewMapper() {
		super(OrganizationBasicView.class, Organization.class);
	}

	
	@Override
	public OrganizationBasicView mapFrom(OrganizationBasicView organizationView, Organization organization) {
		
		organizationView = super.mapFrom(organizationView, organization);
		
		organizationView.setRelativeUrl(organization.getRelativeUrl());
		
		return organizationView;
	}
	
	@Override
	public Organization mapTo(OrganizationBasicView organizationView, Organization organization) {

		throw new NotImplementedException();
	}
}
