package org.sklsft.connect.bc.security.token;

import java.util.Date;

import javax.inject.Inject;

import org.sklsft.commons.rest.security.tokens.encoder.TokenEncoder;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.bc.rights.OrganizationRoleManager;
import org.sklsft.connect.bc.security.credentials.UserSecurityCredentials;
import org.sklsft.social.model.account.UserAccount;
import org.springframework.stereotype.Component;

/**
 * A simple factory to build a {@link TokenView} from a
 * <li> {@link UserAccount}
 * @author Nicolas Thibault
 *
 */
@Component
public class TokenFactory {
	
	@Inject
	private OrganizationRoleManager organizationRoleManager;
	
	@Inject 
	private TokenEncoder<UserSecurityCredentials> securityCredentialsEncoder;
	
	
	public String buildToken (UserAccount userAccount) {
		
		UserSecurityCredentials credentials = buildCredentials(userAccount);
		
		return securityCredentialsEncoder.encode(credentials);
	}
	
	
	public TokenView buildTokenView (UserAccount userAccount) {
		
		TokenView tokenView = new TokenView();
		String token = buildToken(userAccount);
		tokenView.setToken(token);
		tokenView.setAccountId(userAccount.getId());
		tokenView.setUserRole(userAccount.getUserRole());
		
		return tokenView;
	}
	
	
	private UserSecurityCredentials buildCredentials (UserAccount user) {
		UserSecurityCredentials credentials = new UserSecurityCredentials();
		credentials.setAccountId(user.getId());			
		credentials.setAccountCreationDate(user.getCreationDate());
		credentials.setCreationDate(new Date());
		return credentials;
	}
}
