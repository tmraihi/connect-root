package org.sklsft.connect.bc.rights.organization;

import javax.inject.Inject;

import org.sklsft.connect.bc.rights.OrganizationRoleManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.util.roles.OrganizationRole;
import org.springframework.stereotype.Component;

@Component
public class OrganizationRightsManager {
	
	@Inject
	private OrganizationRoleManager organizationRoleManager;
	

	public void checkAccessEnabled(UserAccount userAccount, Organization organization) {
		
		organizationRoleManager.checkHasMinimumRole(userAccount, organization, OrganizationRole.MEMBER);
		
	}
	
	
	public void checkUpdateEnabled(UserAccount userAccount, Organization organization) {

		organizationRoleManager.checkHasMinimumRole(userAccount, organization, OrganizationRole.ADMIN);
		
	}


	public void checkDeleteEnabled(UserAccount userAccount, Organization organization) {
		organizationRoleManager.checkHasMinimumRole(userAccount, organization, OrganizationRole.OWNER);
	}
}
