package org.sklsft.connect.bc.mapper.organization;

import javax.inject.Inject;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.commons.mapper.interfaces.Mapper;
import org.sklsft.connect.api.model.organization.of.OrganizationCodeForm;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.sklsft.connect.api.model.organization.ov.OrganizationFullView;
import org.sklsft.connect.bc.mapper.account.AccountBasicViewMapper;
import org.sklsft.connect.bc.security.UserContextHolder;
import org.sklsft.social.model.organization.Organization;
import org.springframework.stereotype.Component;

@Component
public class OrganizationFullViewMapper extends BasicMapperImpl<OrganizationFullView, Organization> implements Mapper<OrganizationFullView, Organization> {

	public OrganizationFullViewMapper() {
		super(OrganizationFullView.class, Organization.class);
	}
	
	@Inject
	private OrganizationFormMapper organizationFormMapper;

	@Inject
	private AccountBasicViewMapper accountPublicFullViewMapper;
	
	@Inject
	private UserContextHolder userContextHolder;
	
	
	
	@Override
	public OrganizationFullView mapFrom(OrganizationFullView organizationView,
			Organization organization) {

		organizationView = super.mapFrom(organizationView, organization);
		
		OrganizationCodeForm organizationCodeForm = new OrganizationCodeForm();
		organizationCodeForm.setCode(organization.getCode());
		organizationView.setOrganizationCodeForm(organizationCodeForm);
		organizationView.setOrganizationForm(organizationFormMapper.mapFrom(new OrganizationForm(), organization));
		organizationView.setRelativeUrl(organization.getRelativeUrl());
		
		return organizationView;
	}

	@Override
	public Organization mapTo(OrganizationFullView organizationView,
			Organization organization) {

		throw new NotImplementedException();
	}
}
