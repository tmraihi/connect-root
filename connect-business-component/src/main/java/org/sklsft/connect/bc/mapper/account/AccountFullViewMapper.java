package org.sklsft.connect.bc.mapper.account;

import javax.inject.Inject;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.AbstractMapper;
import org.sklsft.connect.api.model.account.of.MailForm;
import org.sklsft.connect.api.model.account.of.PersonalDataForm;
import org.sklsft.connect.api.model.account.ov.AccountFullView;
import org.sklsft.social.model.account.UserAccount;
import org.springframework.stereotype.Component;

@Component
public class AccountFullViewMapper extends AbstractMapper<AccountFullView, UserAccount>{
	
	public AccountFullViewMapper() {
		super(AccountFullView.class, UserAccount.class);
	}

	@Inject
	private PersonalDataFormMapper personalDataFormMapper;
	
	
	
	@Override
	public AccountFullView mapFrom(AccountFullView accountFullView, UserAccount userAccount) {
		
		accountFullView.setPersonalData(personalDataFormMapper.mapFrom(new PersonalDataForm(), userAccount));
		accountFullView.setEmailForm(new MailForm());
		accountFullView.getEmailForm().setEmail(userAccount.getEmail());
		accountFullView.setRelativeUrl(userAccount.getRelativeUrl());
		
		return accountFullView;
	}
	
	@Override
	public UserAccount mapTo(AccountFullView accountFullView, UserAccount userAccount) {
		
		throw new NotImplementedException();
	}

}
