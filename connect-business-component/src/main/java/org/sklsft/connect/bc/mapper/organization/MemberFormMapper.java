package org.sklsft.connect.bc.mapper.organization;

import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.util.roles.OrganizationRole;
import org.springframework.stereotype.Component;

@Component
public class MemberFormMapper extends BasicMapperImpl<MemberForm, OrganizationMember> {

	public MemberFormMapper() {
		super(MemberForm.class, OrganizationMember.class);
	}
	
	
	@Override
	public MemberForm mapFrom(MemberForm form, OrganizationMember organizationMember) {
		
		form = super.mapFrom(form, organizationMember);
		form.setRole(OrganizationRole.valueOf(organizationMember.getRole()));
		
		return form;
	}
	
	
	@Override
	public OrganizationMember mapTo(MemberForm form, OrganizationMember organizationMember) {
		
		organizationMember = super.mapTo(form, organizationMember);
		organizationMember.setRole(form.getRole().name());
		
		
		return organizationMember;
	}
}
