package org.sklsft.connect.bc.mapper.organization;

import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.util.roles.OrganizationRole;
import org.springframework.stereotype.Component;

@Component
public class MemberFormFromGuestMapper extends BasicMapperImpl<MemberForm, OrganizationGuest> {

	public MemberFormFromGuestMapper() {
		super(MemberForm.class, OrganizationGuest.class);
	}
	
	
	@Override
	public MemberForm mapFrom(MemberForm form, OrganizationGuest guest) {
		
		form = super.mapFrom(form, guest);
		form.setRole(OrganizationRole.valueOf(guest.getRole()));
		
		return form;
	}
	
	
	@Override
	public OrganizationGuest mapTo(MemberForm form, OrganizationGuest guest) {
		
		guest = super.mapTo(form, guest);
		guest.setRole(form.getRole().name());
		
		
		return guest;
	}
}
