package org.sklsft.connect.bc.security.credentials.validator;

import org.sklsft.commons.rest.security.credentials.validator.SecurityCredentialsValidator;
import org.sklsft.connect.bc.security.credentials.ApplicationSecurityCredentials;
import org.springframework.stereotype.Component;

@Component("applicationSecurityCredentialsValidator")
public class ApplicationSecurityCrendentialsValidator implements SecurityCredentialsValidator<ApplicationSecurityCredentials> {

	@Override
	public void validateCredentials(ApplicationSecurityCredentials securityCredentials) {
		
	}
}
