package org.sklsft.connect.bc.mail.context.account;

import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.util.mail.context.MailContext;

/**
 * an url must be delivered with this mail to validate mail
 * {@see MailContext}
 * @author Nicolas Thibault
 *
 */
public class ValidationMailContext extends MailContext {
	
	/*
	 * properties
	 */
	private String validationUrl;

	
	/*
	 * getters and setters
	 */
	public String getValidationUrl() {
		return validationUrl;
	}

	public void setValidationUrl(String validationUrl) {
		this.validationUrl = validationUrl;
	}
	
	
	/*
	 * constructor
	 */
	public ValidationMailContext(UserAccount userAccount, String subject, String validationUrl) {
		super(userAccount.getEmail(), subject);
		this.validationUrl = validationUrl;
	}
}
