package org.sklsft.connect.bc.rights;

import javax.inject.Inject;

import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationMemberDao;
import org.sklsft.social.util.roles.OrganizationRole;
import org.springframework.stereotype.Component;

@Component
public class OrganizationRoleManager {

	@Inject
	private OrganizationMemberDao organizationMemberDao;
	
	public OrganizationRole getRole(UserAccount userAccount, Organization organization) {
		
		OrganizationMember member = organizationMemberDao.loadOrganizationMemberByOrganizationIdAndUserId(organization.getId(), userAccount.getId());
		if (member == null) {
			return null;
		}
		
		return OrganizationRole.valueOf(member.getRole());
	}
	
	
	public boolean hasMinimumRole (UserAccount userAccount, Organization organization, OrganizationRole role) {
		OrganizationRole currentRole = getRole(userAccount, organization);
		if (currentRole == null) {
			return false;
		}
		if (currentRole.getPriority()>role.getPriority()) {
			return false;
		}
		return true;
	}
	
	
	public void checkHasMinimumRole (UserAccount userAccount, Organization organization, OrganizationRole role) {
		if (!hasMinimumRole(userAccount, organization, role)) {
			throw new OperationDeniedException("operation.impossible");
		}
	}
}
