package org.sklsft.connect.bc.retriever.organization;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.sklsft.connect.api.model.organization.ov.GuestView;
import org.sklsft.connect.api.model.organization.ov.MemberView;
import org.sklsft.connect.bc.mapper.account.AccountPublicBasicViewMapper;
import org.sklsft.connect.bc.mapper.organization.GuestViewMapper;
import org.sklsft.connect.bc.mapper.organization.MemberViewMapper;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationGuestDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationMemberDao;
import org.springframework.stereotype.Component;

@Component
public class OrganizationMembershipRetriever {
	
	@Inject
	private OrganizationMemberDao organizationMemberDao;
	
	@Inject
	private OrganizationGuestDao organizationGuestDao;
	
	@Inject
	private AccountPublicBasicViewMapper accountPublicBasicViewMapper;
	
	@Inject
	private MemberViewMapper memberViewMapper;
	
	@Inject
	private GuestViewMapper guestViewMapper;
	

	public List<MemberView> getMembers(Long organizationId) {
		
		List<MemberView> result = new ArrayList<>();
		
		List<OrganizationMember> members = organizationMemberDao.loadListFromOrganization(organizationId);
		
		for (OrganizationMember member:members) {
			result.add(memberViewMapper.mapFrom(member));
		}
		
		return result;
	}
	
	public List<GuestView> getGuests(Long organizationId) {
		
		List<GuestView> result = new ArrayList<>();
		
		List<OrganizationGuest> members = organizationGuestDao.loadListFromOrganization(organizationId);
		
		for (OrganizationGuest member:members) {
			result.add(guestViewMapper.mapFrom(member));
		}
		
		return result;
	}
}
