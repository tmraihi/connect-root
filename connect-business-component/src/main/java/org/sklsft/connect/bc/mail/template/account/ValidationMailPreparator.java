package org.sklsft.connect.bc.mail.template.account;

import java.util.List;

import org.sklsft.social.util.mail.model.InlineAttachment;
import org.sklsft.social.util.mail.template.UserAccountMailPreparator;
import org.springframework.stereotype.Component;

@Component
public class ValidationMailPreparator extends UserAccountMailPreparator{

	@Override
	protected List<InlineAttachment> getAttachements() {
		
		return null;
	}

}
