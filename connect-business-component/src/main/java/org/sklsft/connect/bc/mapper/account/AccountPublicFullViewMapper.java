package org.sklsft.connect.bc.mapper.account;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.account.ov.AccountPublicFullView;
import org.sklsft.social.model.account.UserAccount;
import org.springframework.stereotype.Component;

@Component
public class AccountPublicFullViewMapper extends BasicMapperImpl<AccountPublicFullView, UserAccount> {
		
	public AccountPublicFullViewMapper() {
		super(AccountPublicFullView.class, UserAccount.class);
	}

	@Override
	public AccountPublicFullView mapFrom(AccountPublicFullView accountPublicFullView, UserAccount userAccount) {
		
		accountPublicFullView = super.mapFrom(accountPublicFullView, userAccount);
		accountPublicFullView.setRelativeUrl(userAccount.getRelativeUrl());
		
		return accountPublicFullView;
	}
	
	
	@Override
	public UserAccount mapTo(AccountPublicFullView accountPublicFullView, UserAccount userAccount) {
		
		throw new NotImplementedException();
	}

}
