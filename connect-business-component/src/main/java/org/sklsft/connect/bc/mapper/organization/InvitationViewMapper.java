package org.sklsft.connect.bc.mapper.organization;

import javax.inject.Inject;

import org.apache.commons.lang.NotImplementedException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.connect.api.model.organization.ov.InvitationView;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.springframework.stereotype.Component;


@Component
public class InvitationViewMapper extends BasicMapperImpl<InvitationView, OrganizationGuest> {

	public InvitationViewMapper() {
		super(InvitationView.class, OrganizationGuest.class);
	}
	
	@Inject
	private OrganizationBasicViewMapper organizationBasicViewMapper;
	
	@Inject
	private MemberFormFromGuestMapper memberFormMapper;
	
		
	
	@Override
	public InvitationView mapFrom(InvitationView view, OrganizationGuest guest) {
		
		view = super.mapFrom(view, guest);
		
		view.setOrganization(organizationBasicViewMapper.mapFrom(guest.getOrganization()));
		view.setMember(memberFormMapper.mapFrom(guest));
		
		return view;
	}
	
	
	@Override
	public OrganizationGuest mapTo(InvitationView view, OrganizationGuest organizationMember) {
		
		throw new NotImplementedException();
	}
}
