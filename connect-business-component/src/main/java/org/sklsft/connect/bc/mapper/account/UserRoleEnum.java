package org.sklsft.connect.bc.mapper.account;

/**
 * Created by M431699 on 29/02/2020.
 */
public enum UserRoleEnum {

    USER_CANDIDATE_ROLE("CANDIDATE_ROLE"),
    USER_FACULTY_ROLE("FACULTY_ROLE"),
    USER_ADMIN_ALL_ROLE("ADMIN_ALL_ROLE");
    private String userRole;

     UserRoleEnum(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRole() {
        return userRole;

    }

}
