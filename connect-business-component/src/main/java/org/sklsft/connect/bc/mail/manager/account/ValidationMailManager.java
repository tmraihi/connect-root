package org.sklsft.connect.bc.mail.manager.account;

import javax.inject.Inject;
import javax.mail.MessagingException;

import org.sklsft.connect.bc.mail.context.account.ValidationMailContext;
import org.sklsft.connect.bc.mail.template.account.ValidationMailPreparator;
import org.sklsft.connect.bc.security.token.TokenFactory;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.util.mail.model.UserAccountMail;
import org.sklsft.social.util.mail.model.UserAccountMailFactory;
import org.sklsft.social.util.mail.sender.interfaces.UserAccountMailSender;


public class ValidationMailManager {
	
	/*
	 * this url has to be injected via a jndi variable in xml
	 */
	private String webServerUrl;

	public void setWebServerUrl(String webServerUrl) {
		this.webServerUrl = webServerUrl;
	}

	@Inject
	private TokenFactory tokenFactory;
	
	@Inject
	private ValidationMailPreparator mailPreparator;
	
	@Inject
	private UserAccountMailFactory userAccountMailFactory;
	
	@Inject
	private UserAccountMailSender mailSender;
	
	public void sendActivationMail(UserAccount userAccount) throws MessagingException {
		
		String subject = getSubject();
		String validationUrl = getValidationUrl(userAccount);
		
		ValidationMailContext mailContext = new ValidationMailContext(userAccount, subject, validationUrl);
		
		UserAccountMail mail = mailPreparator.prepareMail(mailContext, userAccountMailFactory);
		
		mailSender.sendMail(mail);
	}

	private String getValidationUrl(UserAccount userAccount) {
		
		String token = tokenFactory.buildToken(userAccount);
		return webServerUrl + "/public/validation.jsf?token=" + token;
	}

	private String getSubject() {
		return "Dernière étape : validez votre inscription";
	}

}
