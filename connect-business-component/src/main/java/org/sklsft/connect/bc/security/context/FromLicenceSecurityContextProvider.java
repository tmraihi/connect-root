package org.sklsft.connect.bc.security.context;

import javax.inject.Inject;

import org.sklsft.commons.rest.security.context.impl.FromKeySecurityContextProvider;
import org.sklsft.commons.rest.security.credentials.validator.SecurityCredentialsValidator;
import org.sklsft.connect.bc.security.credentials.ApplicationSecurityCredentials;
import org.springframework.stereotype.Component;

@Component
public class FromLicenceSecurityContextProvider extends FromKeySecurityContextProvider<ApplicationSecurityCredentials>{

	@Inject
	public FromLicenceSecurityContextProvider(SecurityCredentialsValidator<ApplicationSecurityCredentials> credentialsValidator) {
		super(credentialsValidator);
	}

	@Override
	protected ApplicationSecurityCredentials get(String token) {
		return new ApplicationSecurityCredentials();
	}

}
