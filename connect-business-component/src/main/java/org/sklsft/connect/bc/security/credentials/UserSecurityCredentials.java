package org.sklsft.connect.bc.security.credentials;

import java.io.Serializable;
import java.util.Date;


/**
 * This class represents the security context. It contains : 
 * <li>the id that fully qualifies a end user
 * <li>the account creation date that can be considered as a random key
 * <li>the token creation date to check its validity
 * @author Nicolas Thibault
 *
 */
public class UserSecurityCredentials implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/*
	 * properties
	 */
	private Long accountId;
	private Date accountCreationDate;
	private Date creationDate;

	
	/*
	 * getters and setters
	 */
	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Date getAccountCreationDate() {
		return accountCreationDate;
	}

	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
