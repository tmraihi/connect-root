package org.sklsft.connect.mvc.controller;



import java.util.Date;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.sklsft.commons.api.exception.ApplicationException;
import org.sklsft.commons.api.exception.TechnicalError;
import org.sklsft.connect.api.model.resources.of.AttachmentForm;
import org.sklsft.connect.api.model.resources.ov.AttachmentView;
import org.sklsft.connect.client.file.FileService;
import org.sklsft.social.util.text.DurationUtils;
import org.springframework.stereotype.Controller;

@Controller
public  class CommonController extends BaseController {
	
	@Inject
	private FileService fileService;
		
		
	
	public String getRestUrl(){
		return getJndiVar("restServerUrl");
	}
	
	
	public String getTimeMillis() {
		return String.valueOf(new Date().getTime());
	}
	
	
	public String getDuration(Date date) {
		return DurationUtils.getDuration(date);
	}
	
	
	public String openFile(AttachmentView attachment) {

		ServletOutputStream servletOutputStream = null;

		try {

			byte[] data = fileService.getFileContent(attachment.getRelativeUrl());

			HttpServletResponse response = (HttpServletResponse) FacesContext
					.getCurrentInstance().getExternalContext().getResponse();
			response.setContentType("application/octet-stream");

			response.setHeader(
					"Content-Disposition",
					"attachment;filename=" + attachment.getAttachmentName());

			servletOutputStream = response.getOutputStream();

			for (byte b : data) {
				servletOutputStream.write(b);
			}

			servletOutputStream.flush();
			servletOutputStream.close();

			response.flushBuffer();
			FacesContext.getCurrentInstance().responseComplete();

			return null;

		} catch (ApplicationException e) {
			displayError(e.getMessage());
			return null;
		} catch (Exception e) {
			displayError(TechnicalError.ERROR_UNKNOWN);
			return null;
		} finally {
			try {
				if (servletOutputStream != null) {
					servletOutputStream.close();
				}
			} catch (Exception e) {

			}
		}
	}
	
	
	public String openFile(AttachmentForm attachment) {

		ServletOutputStream servletOutputStream = null;

		try {

			byte[] data = attachment.getData();

			HttpServletResponse response = (HttpServletResponse) FacesContext
					.getCurrentInstance().getExternalContext().getResponse();
			response.setContentType("application/octet-stream");

			response.setHeader(
					"Content-Disposition",
					"attachment;filename=" + attachment.getAttachmentName());

			servletOutputStream = response.getOutputStream();

			for (byte b : data) {
				servletOutputStream.write(b);
			}

			servletOutputStream.flush();
			servletOutputStream.close();

			response.flushBuffer();
			FacesContext.getCurrentInstance().responseComplete();

			return null;

		} catch (ApplicationException e) {
			displayError(e.getMessage());
			return null;
		} catch (Exception e) {
			displayError(TechnicalError.ERROR_UNKNOWN);
			return null;
		} finally {
			try {
				if (servletOutputStream != null) {
					servletOutputStream.close();
				}
			} catch (Exception e) {

			}
		}
	}	
}