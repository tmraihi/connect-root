package org.sklsft.connect.mvc.controller.organization;

import javax.inject.Inject;

import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.connect.api.interfaces.organization.OrganizationService;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.organization.OrganizationCreationView;
import org.sklsft.social.util.pages.Page;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class OrganizationCreationController extends BaseController {

	/*
	 * the view(s) handled by the controller
	 */
	@Inject
	private OrganizationCreationView organizationCreationView;

	/*
	 * the service(s) used by the controller
	 */
	@Inject
	private OrganizationService organizationService;


	public void prepareOrganizationCreation() {

		organizationCreationView.setOrganizationForm(new OrganizationForm());
	}

	public void createOrganization() {

		executeAjaxMethod("organization.create", new AjaxMethodTemplate() {
			@Override
			public Object execute() {
				return organizationService.createOrganization(organizationCreationView.getOrganizationForm());
			}

			@Override
			public void redirectOnComplete(Object result) {
				redirect(Page.ORGANIZATION_DETAIL.getUrl() + "?id=" + result.toString());
			}
		});
	}

}
