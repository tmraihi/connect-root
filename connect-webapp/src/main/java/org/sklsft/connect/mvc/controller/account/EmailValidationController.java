package org.sklsft.connect.mvc.controller.account;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.sklsft.connect.api.exception.account.EmailAlreadyActivatedException;
import org.sklsft.connect.api.exception.account.ValidationEmailExpiredException;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.account.EmailValidationView;
import org.sklsft.connect.mvc.security.SecurityController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class EmailValidationController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailValidationController.class);

	/*
	 * the view(s) handled by the controller
	 */
	@Inject
	private EmailValidationView emailValidationView;
	
	
	/*
	 * the service(s) used by the controller
	 */
	@Inject
	private AccountService accountService;
	
	@Inject
	private SecurityController securityController;
	
	
	@PostConstruct
	public void init() {
		String token = extractToken();
		try {
			logger.info("validating mail for token " + token);
			TokenView tokenView = accountService.activateEmail(token);
			securityController.provideSecurityContext(tokenView);
			logger.info("completed");
			emailValidationView.setValidationSuccessfull(true);
		} catch (EmailAlreadyActivatedException e) {
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage());
			emailValidationView.setEmailAlreadyValidated(true);
		} catch (ValidationEmailExpiredException e) {
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage());
			emailValidationView.setEmailExpired(true);
		} catch (Exception e) {
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage());
			emailValidationView.setEmailInvalid(true);
		}
		
	}
	
}
