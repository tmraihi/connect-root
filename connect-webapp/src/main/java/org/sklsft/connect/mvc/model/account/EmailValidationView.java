package org.sklsft.connect.mvc.model.account;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class EmailValidationView {
	
	/*
	 * properties
	 */
	private boolean validationSuccessfull = false;
	private boolean emailAlreadyValidated = false;
	private boolean emailExpired =  false;
	private boolean emailInvalid =  false;
	
	
	/*
	 * getters and setters
	 */
	public boolean isValidationSuccessfull() {
		return validationSuccessfull;
	}
	public void setValidationSuccessfull(boolean validationSuccessfull) {
		this.validationSuccessfull = validationSuccessfull;
	}
	public boolean isEmailAlreadyValidated() {
		return emailAlreadyValidated;
	}
	public void setEmailAlreadyValidated(boolean emailAlreadyValidated) {
		this.emailAlreadyValidated = emailAlreadyValidated;
	}
	public boolean isEmailExpired() {
		return emailExpired;
	}
	public void setEmailExpired(boolean emailExpired) {
		this.emailExpired = emailExpired;
	}
	public boolean isEmailInvalid() {
		return emailInvalid;
	}
	public void setEmailInvalid(boolean emailInvalid) {
		this.emailInvalid = emailInvalid;
	}
}
