package org.sklsft.connect.mvc.security;

import java.util.Collection;
import java.util.HashSet;

import org.sklsft.connect.api.interfaces.authentication.AuthenticationService;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * implementation of a {@link Authentication} that uses a token provided by the {@link AuthenticationService}
 * @author Nicolas Thibault
 *
 */
public class ConnectAuthentication implements Authentication {
	
	private static final long serialVersionUID = -4283565091466485717L;
	
	private TokenView tokenView;
	private Collection<GrantedAuthority> authorities;
	
	public ConnectAuthentication(TokenView tokenView) {
		this.tokenView = tokenView;
		authorities = new HashSet<GrantedAuthority>();
		// HERE ADD THE GOOD USER ROLE
		String role = tokenView.getUserRole();
		if(role.equals("USER_ADMIN_ALL_ROLE")){
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		}
		else {
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		}

	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getDetails() {
		return null;
	}

	
	/**
	 * the token is accessible as the Principal as it fully qualifies the end user
	 */
	@Override
	public TokenView getPrincipal() {
		return tokenView;
	}

	@Override
	public boolean isAuthenticated() {
		return true;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated)
			throws IllegalArgumentException {
		
	}
}
