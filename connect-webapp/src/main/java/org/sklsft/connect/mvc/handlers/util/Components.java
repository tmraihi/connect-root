package org.sklsft.connect.mvc.handlers.util;

import static java.util.regex.Pattern.quote;

import java.util.Map;

import javax.faces.component.UICommand;
import javax.faces.component.UIComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

/**
 * 
 * From Omnifaces, special thanks to BalusC
 *
 */
public class Components {

	public static boolean hasInvokedSubmit(UIComponent component) {
		UIComponent source = getCurrentActionSource();
		return source != null && source.equals(component);
	}

	public static UIComponent getCurrentActionSource() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (!context.isPostback()) {
			return null;
		}

		UIViewRoot viewRoot = context.getViewRoot();
		Map<String, String> params = context.getExternalContext().getRequestParameterMap();
		UIComponent actionSource = null;

		if (context.getPartialViewContext().isAjaxRequest()) {
			String sourceClientId = params.get("javax.faces.source");

			if (sourceClientId != null) {
				actionSource = findComponentIgnoringIAE(viewRoot, stripIterationIndexFromClientId(sourceClientId));
			}
		}

		if (actionSource == null) {
			for (String name : params.keySet()) {
				if (name.startsWith("javax.faces.")) {
					continue; // Quick skip.
				}

				actionSource = findComponentIgnoringIAE(viewRoot, stripIterationIndexFromClientId(name));

				if (actionSource instanceof UICommand) {
					break;
				}
			}
		}

		return actionSource;
	}
	
	
	private static String stripIterationIndexFromClientId(String clientId) {
		String separatorChar = Character.toString(UINamingContainer.getSeparatorChar(FacesContext.getCurrentInstance()));
		return clientId.replaceAll(quote(separatorChar) + "[0-9]+" + quote(separatorChar), separatorChar);
	}
	
	
	private static UIComponent findComponentIgnoringIAE(UIViewRoot viewRoot, String clientId) {
		try {
			return viewRoot.findComponent(clientId);
		}
		catch (IllegalArgumentException ignore) {
			// May occur when view has changed by for example a successful navigation.
			return null;
		}
	}

}