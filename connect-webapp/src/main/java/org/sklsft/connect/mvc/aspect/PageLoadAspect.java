package org.sklsft.connect.mvc.aspect;

import java.lang.reflect.Method;
import java.util.MissingResourceException;

import javax.inject.Inject;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.connect.api.exception.authentication.AccountLockedException;
import org.sklsft.connect.api.exception.authentication.InvalidTokenException;
import org.sklsft.connect.api.exception.authentication.TokenExpiredException;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.account.ov.AccountBasicView;
import org.sklsft.connect.mvc.model.home.IndexView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * Aspect for intercepting error in JSF viewaction methods
 *
 * @author AELJ
 *
 */
@Aspect
@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class PageLoadAspect {

	private static final Logger logger = LoggerFactory.getLogger(PageLoadAspect.class);

	/*
	 * the view(s) used by the aspect
	 */

	@Inject
	private IndexView indexView;

	/*
	 * the service(s) used by the aspect
	 */
	@Inject
	private RedirectExceptionHandler redirectExceptionHandler;
	@Inject
	private AccountService accountService;

	/*
	 * the controller(s) used by the aspect
	 */
	@Around("@annotation(org.sklsft.connect.mvc.aspect.PageLoad)")
	public void execute(ProceedingJoinPoint joinPoint) throws Throwable {
		try {
			if (isAuthenticationRequired(joinPoint)) {
				refreshMyAccount();
			}

			joinPoint.proceed();

		} catch (MissingResourceException e) {
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
			redirectExceptionHandler.redirectOnMissingResource();
		} catch (AccessDeniedException e) {
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
			redirectExceptionHandler.redirectOnAccessDenied();
		} catch (InvalidTokenException | TokenExpiredException e) {
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
			redirectExceptionHandler.redirectOnInvalidToken();
		} catch (AccountLockedException e) {
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
			redirectExceptionHandler.redirectOnAccountLocked();
//		} catch (AccountDeletedException e) {
//			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
//			redirectExceptionHandler.redirectOnAccountDeleted();
		} catch (Exception e) {
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
			redirectExceptionHandler.redirectOnException();
		}
	}

	private boolean isAuthenticationRequired(ProceedingJoinPoint joinPoint) {
		Method proxiedMethod = ((MethodSignature) joinPoint.getSignature()).getMethod();
		PageLoad pageLoadAnotation = proxiedMethod.getAnnotation(PageLoad.class);
		return pageLoadAnotation.authenticationRequired();
	}

	private void refreshMyAccount() {
		AccountBasicView myAccount = accountService.retrieveMyAccount();
		indexView.setAccount(myAccount);
	}
}
