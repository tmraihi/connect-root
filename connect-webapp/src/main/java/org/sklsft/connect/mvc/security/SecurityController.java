package org.sklsft.connect.mvc.security;

import javax.inject.Inject;

import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.home.IndexView;
import org.sklsft.social.util.pages.Page;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class SecurityController extends BaseController {
	
	@Inject
	private ConnectSecurityContextProvider connectSecurityContextProvider;
	
	@Inject
	private IndexView indexView;
	
	
	public void provideSecurityContext(TokenView tokenView) {
		connectSecurityContextProvider.provideSecurityContext(tokenView);
		indexView.setToken(tokenView.getToken());
	}
	
	
	public void checkContextChange() {
		TokenView tokenView = getTokenView();
		if (tokenView != null) {
			if (!tokenView.getToken().equals(indexView.getToken())) {
				indexView.setToken(tokenView.getToken());
				displayInfo("context.changed");
				redirect(Page.PRIVATE_INDEX.getUrl());
			}			
		} else {
			redirect(Page.PUBLIC_INDEX.getUrl());
		}
	}
	
	private TokenView getTokenView() {
		
		if (SecurityContextHolder.getContext().getAuthentication() != null) {
			if (!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
				return (TokenView) SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal();
			}
		}
		return null;
	}
	
	public void clearSecurityContext() {
		connectSecurityContextProvider.clearSecurityContext();
	}
}
