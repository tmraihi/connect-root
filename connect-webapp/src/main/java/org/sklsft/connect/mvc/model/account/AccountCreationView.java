package org.sklsft.connect.mvc.model.account;

import java.io.Serializable;

import org.sklsft.connect.api.model.account.of.AccountCreationForm;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class AccountCreationView implements Serializable {

	private static final long serialVersionUID = -7631498525228500664L;
	
	/*
	 * properties
	 */
	private AccountCreationForm accountCreationForm = new AccountCreationForm();
	
	
	
	/*
	 * getters and setters
	 */
	public AccountCreationForm getAccountCreationForm() {
		return accountCreationForm;
	}
	public void setAccountCreationForm(AccountCreationForm accountCreationForm) {
		this.accountCreationForm = accountCreationForm;
	}
}
