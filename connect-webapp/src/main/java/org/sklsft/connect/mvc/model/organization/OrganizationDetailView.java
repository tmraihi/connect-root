package org.sklsft.connect.mvc.model.organization;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.servlet.http.Part;

import org.sklsft.commons.mvc.scopes.ViewScope;
import org.sklsft.connect.api.model.account.ov.AccountPublicBasicView;
import org.sklsft.connect.api.model.account.ov.AccountPublicFullView;
import org.sklsft.connect.api.model.organization.of.GuestForm;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.connect.api.model.organization.ov.GuestView;
import org.sklsft.connect.api.model.organization.ov.MemberView;
import org.sklsft.connect.api.model.organization.ov.OrganizationFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ViewScope.NAME)
public class OrganizationDetailView implements Serializable {

	private static final long serialVersionUID = -1985728901926818898L;

	/*
	 * properties
	 */
	private OrganizationFullView organizationFullView = new OrganizationFullView();
	
	private List<MemberView> members;
	private List<GuestView> guests;
	
	private AccountPublicBasicView targetMember;
	private MemberForm targetMemberForm;
	
	private GuestForm inviteMemberForm;
	
	private List<SelectItem> roles;
	
	private boolean codeEditable = false;
	private boolean codeAlreadyExists = false;
	
	private Part logo;
	

	/*
	 * getters and setters
	 */
	public OrganizationFullView getOrganizationFullView() {
		return organizationFullView;
	}
	public void setOrganizationFullView(OrganizationFullView organizationFullView) {
		this.organizationFullView = organizationFullView;
	}
	public List<MemberView> getMembers() {
		return members;
	}
	public void setMembers(List<MemberView> members) {
		this.members = members;
	}
	public List<GuestView> getGuests() {
		return guests;
	}
	public void setGuests(List<GuestView> guests) {
		this.guests = guests;
	}
	public AccountPublicBasicView getTargetMember() {
		return targetMember;
	}
	public void setTargetMember(AccountPublicBasicView targetMember) {
		this.targetMember = targetMember;
	}
	public MemberForm getTargetMemberForm() {
		return targetMemberForm;
	}
	public void setTargetMemberForm(MemberForm targetMemberForm) {
		this.targetMemberForm = targetMemberForm;
	}
	public GuestForm getInviteMemberForm() {
		return inviteMemberForm;
	}
	public void setInviteMemberForm(GuestForm inviteMemberForm) {
		this.inviteMemberForm = inviteMemberForm;
	}
	public List<SelectItem> getRoles() {
		return roles;
	}
	public void setRoles(List<SelectItem> roles) {
		this.roles = roles;
	}
	public boolean isCodeEditable() {
		return codeEditable;
	}
	public void setCodeEditable(boolean codeEditable) {
		this.codeEditable = codeEditable;
	}
	public boolean isCodeAlreadyExists() {
		return codeAlreadyExists;
	}
	public void setCodeAlreadyExists(boolean codeAlreadyExists) {
		this.codeAlreadyExists = codeAlreadyExists;
	}
	public Part getLogo() {
		return logo;
	}
	public void setLogo(Part logo) {
		this.logo = logo;
	}
}
