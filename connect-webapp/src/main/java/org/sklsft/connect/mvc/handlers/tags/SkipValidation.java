package org.sklsft.connect.mvc.handlers.tags;

import static java.lang.Boolean.TRUE;
import static javax.faces.event.PhaseId.RESTORE_VIEW;
import static org.sklsft.connect.mvc.handlers.util.Components.hasInvokedSubmit;
import static org.sklsft.connect.mvc.handlers.util.Events.subscribeToRequestAfterPhase;
import static org.sklsft.connect.mvc.handlers.util.Events.subscribeToViewEvent;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.el.ValueExpression;
import javax.faces.component.UICommand;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PostValidateEvent;
import javax.faces.event.PreValidateEvent;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;
import javax.faces.validator.Validator;
import javax.faces.view.facelets.ComponentHandler;
import javax.faces.view.facelets.FaceletContext;
import javax.faces.view.facelets.TagConfig;
import javax.faces.view.facelets.TagHandler;

import org.sklsft.connect.mvc.handlers.util.Callback;


/**
 * 
 * From Omnifaces, special thanks to BalusC
 * Used to tell a request to bypass validation
 *
 */
public class SkipValidation extends TagHandler {

	// Constants ------------------------------------------------------------------------------------------------------

	private static final String ERROR_INVALID_PARENT =
		"Parent component of o:skipValidators must be an instance of UICommand or ClientBehaviorHolder.";

	// Constructors ---------------------------------------------------------------------------------------------------

	/**
	 * The tag constructor.
	 * @param config The tag config.
	 */
	public SkipValidation(TagConfig config) {
		super(config);
	}

	// Actions --------------------------------------------------------------------------------------------------------

	/**
	 * If the parent component is an instance of {@link UICommand} or {@link ClientBehaviorHolder}, and is new, and
	 * we're in the restore view phase of a postback, then delegate to {@link #processSkipValidators(UIComponent)}.
	 * @throws IllegalStateException When the parent component is not an instance of {@link UICommand} or
	 * {@link ClientBehaviorHolder}.
	 */
	@Override
	public void apply(FaceletContext context, final UIComponent parent) throws IOException {
		if (!(parent instanceof UICommand || parent instanceof ClientBehaviorHolder)) {
			throw new IllegalStateException(ERROR_INVALID_PARENT);
		}

		FacesContext facesContext = context.getFacesContext();

		if (!(ComponentHandler.isNew(parent) && facesContext.isPostback() && facesContext.getCurrentPhaseId() == RESTORE_VIEW)) {
			return;
		}

		// We can't use hasInvokedSubmit() before the component is added to view, because the client ID isn't available.
		// Hence, we subscribe this check to after phase of restore view.
		subscribeToRequestAfterPhase(RESTORE_VIEW, new Callback.Void() { @Override public void invoke() {
			processSkipValidators(parent);
		}});
	}

	/**
	 * Check if the given component has been invoked during the current request and if so, then register the skip
	 * validators event listener which removes the validators during {@link PreValidateEvent} and restores them during
	 * {@link PostValidateEvent}.
	 * @param parent The parent component of this tag.
	 */
	protected void processSkipValidators(UIComponent parent) {
		if (!hasInvokedSubmit(parent)) {
			return;
		}

		SkipValidatorsEventListener listener = new SkipValidatorsEventListener();
		subscribeToViewEvent(PreValidateEvent.class, listener);
		subscribeToViewEvent(PostValidateEvent.class, listener);
	}

	/**
	 * Remove validators during prevalidate and restore them during postvalidate.
	 */
	static class SkipValidatorsEventListener implements SystemEventListener {

		private Map<String, Object> required = new HashMap<>();
		private Map<String, Validator[]> allValidators = new HashMap<>();

		@Override
		public boolean isListenerForSource(Object source) {
			return source instanceof UIInput;
		}

		@Override
		public void processEvent(SystemEvent event) throws AbortProcessingException {
			UIInput input = (UIInput) event.getSource();
			String clientId = input.getClientId();

			if (event instanceof PreValidateEvent) {
				ValueExpression requiredExpression = input.getValueExpression("required");
				required.put(clientId, (requiredExpression != null) ? requiredExpression : input.isRequired());
				input.setRequired(false);
				Validator[] validators = input.getValidators();
				allValidators.put(clientId, validators);

				for (Validator validator : validators) {
					input.removeValidator(validator);
				}
			}
			else if (event instanceof PostValidateEvent) {
				Object requiredValue = required.remove(clientId);

				if (requiredValue instanceof ValueExpression) {
					input.setValueExpression("required", (ValueExpression) requiredValue);
				}
				else {
					input.setRequired(TRUE.equals(requiredValue));
				}

				for (Validator validator : allValidators.remove(clientId)) {
					input.addValidator(validator);
				}
			}
		}

	}

}