package org.sklsft.connect.mvc.controller.account;

import java.io.IOException;

import javax.inject.Inject;

import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.controller.authentication.AuthenticationController;
import org.sklsft.connect.mvc.model.account.AccountCreationView;
import org.sklsft.connect.mvc.model.home.IndexView;
import org.sklsft.connect.mvc.security.SecurityController;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class AccountCreationController extends BaseController {

	/*
	 * the view(s) handled by the controller
	 */
	@Inject
	private AccountCreationView accountCreationView;
	
	@Inject
	private IndexView indexView;
	
	
	public AccountCreationView getAccountCreationView() {
		return accountCreationView;
	}
	public void setAccountCreationView(AccountCreationView accountCreationView) {
		this.accountCreationView = accountCreationView;
	}


	/*
	 * the service(s) used by the controller
	 */
	@Inject
	private AccountService accountService;

	@Inject
	private SecurityController securityController;
	
	@Inject
	private AuthenticationController authenticationController;
	
		
	
	
	
	/**
	 * creates an account from the registration form
	 */
	@AjaxMethod(value="account.create")
	public void createAccount() throws IOException {

		TokenView tokenView = accountService.createAccount(accountCreationView.getAccountCreationForm());
		securityController.provideSecurityContext(tokenView);
		indexView.setAccount(accountService.retrieveMyAccount());
		
		authenticationController.accessPrivateSite(tokenView);
	}
}
