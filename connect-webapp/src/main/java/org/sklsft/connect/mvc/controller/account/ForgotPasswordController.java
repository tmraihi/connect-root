package org.sklsft.connect.mvc.controller.account;

import javax.inject.Inject;

import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.account.ForgotPasswordView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class ForgotPasswordController extends BaseController {

	/*
	 * the view(s) handled by the controller
	 */
	@Inject
	private ForgotPasswordView forgotPasswordView;


	/*
	 * the service(s) used by the controller
	 */
	@Inject
	private AccountService accountService;

	@AjaxMethod(value = "password.reset.sent")
	public void sendResetPassword() {
		
		accountService.sendResetPassword(forgotPasswordView.getEmailForm());

	}

}
