package org.sklsft.connect.mvc.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;


/**
 * as a ConnectAuthentication is fully qualified, no specific Authentication is done further more.
 * @author Nicolas Thibault
 *
 */
@Component
public class ConnectAuthenticationProvider implements AuthenticationProvider {
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		return authentication;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.isAssignableFrom(ConnectAuthentication.class);
	}
}
