package org.sklsft.connect.mvc.controller.account;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.sklsft.commons.api.exception.ApplicationException;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.account.of.ResetPasswordForm;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.account.ResetPasswordView;
import org.sklsft.connect.mvc.security.SecurityController;
import org.sklsft.social.util.pages.Page;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class ResetPasswordController extends BaseController {

	/*
	 * the view(s) handled by the controller
	 */
	@Inject
	private ResetPasswordView resetPasswordView;

	public ResetPasswordView getResetPasswordView() {
		return resetPasswordView;
	}

	public void setResetPasswordView(ResetPasswordView resetPasswordView) {
		this.resetPasswordView = resetPasswordView;
	}


	/*
	 * the service(s) used by the controller
	 */
	@Inject
	private AccountService accountService;
	
	@Inject
	private SecurityController securityController;
	


	@PostConstruct
	public void init() {

		String token = extractToken();
		
		resetPasswordView.setResetPasswordForm(new ResetPasswordForm());
		
		resetPasswordView.getResetPasswordForm().setToken(token);
	}


	public void resetPassword() throws IOException {
	
		try {

			logger.info("password.reset");

			TokenView tokenView = accountService.resetPassword(resetPasswordView.getResetPasswordForm());

			securityController.provideSecurityContext(tokenView);
			
			displayInfo("password.reset.success");
			logger.info("completed");

			redirect(Page.PRIVATE_INDEX.getUrl());
		
		} catch (ApplicationException e) {
			displayError(e.getMessage());
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage());
		} catch (Exception e) {
			displayError("password.reset.failure");
			logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage());
		}
	}
}
