package org.sklsft.connect.mvc.model.account;

import org.sklsft.connect.api.model.account.of.MailForm;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class ForgotPasswordView {

	/*
	 * properties
	 */
	private MailForm emailForm = new MailForm();


	/*
	 * getters and setters
	 */
	public MailForm getEmailForm() {
		return emailForm;
	}

	public void setEmailForm(MailForm emailForm) {
		this.emailForm = emailForm;
	}
}
