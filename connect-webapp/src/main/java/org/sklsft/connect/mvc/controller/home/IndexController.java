package org.sklsft.connect.mvc.controller.home;

import javax.inject.Inject;

import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.interfaces.organization.OrganizationMembershipService;
import org.sklsft.connect.api.interfaces.organization.OrganizationService;
import org.sklsft.connect.mvc.aspect.PageLoad;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.home.IndexView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class IndexController extends BaseController {

	/*
	 * the view(s) handled by the controller
	 */
	@Inject
	private IndexView indexView;
	

	/*
	 * the service(s) used by the controller
	 */
	@Inject
	private AccountService accountService;

	@Inject
	private OrganizationService organizationService;
	
	@Inject
	private OrganizationMembershipService organizationMembershipService;
	
	
	private void refreshMyOrganizations() {
		indexView.setMyOrganizations(organizationService.retrieveMyOrganizations());
	}
		
	private void refreshMyInvitations() {
		indexView.setMyInvitations(organizationService.retrieveMyInvitations());
	}
	
	
	@PageLoad
	public void load() {
		refreshMyOrganizations();
		refreshMyInvitations();
	}
	
	
	@AjaxMethod("invitation.accept")
	public void acceptJoinOrganization(Long id) {
		organizationMembershipService.acceptJoinOrganization(id);
		refreshMyOrganizations();
		refreshMyInvitations();
	}
	
	
	@AjaxMethod("invitation.refuse")
	public void refuseJoinOrganization(Long id) {
		organizationMembershipService.refuseJoinOrganization(id);
		refreshMyOrganizations();
		refreshMyInvitations();
	}
	

	@AjaxMethod("validationMail.resend")
	public void resendValidationMail() {
		accountService.resendValidationMail();
	}
}
