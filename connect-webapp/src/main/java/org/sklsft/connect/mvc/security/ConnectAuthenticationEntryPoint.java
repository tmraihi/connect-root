package org.sklsft.connect.mvc.security;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;


/**
 * we define here what happens when spring seurity is not able to read a security context (mainly when no context has been set).<br/>
 * In that case, a public context is given that allows the access to public resources such as the login page.<br/>
 * By default, when no authentication is given, the end user is invited to login.
 * @author Nicolas Thibault
 *
 */
@Component
public class ConnectAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Inject
	ConnectSecurityContextProvider connectSecurityContextProvider;
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
		
		String targetUrl = request.getRequestURL().toString();

		if (request.getQueryString() != null) {
			targetUrl += "?" + request.getQueryString();
		}
		        
        request.getSession().setAttribute("targetUrl", targetUrl);
        
        response.sendError(HttpStatus.FORBIDDEN.value());
        
	}
}
