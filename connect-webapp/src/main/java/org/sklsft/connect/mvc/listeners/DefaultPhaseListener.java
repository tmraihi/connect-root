package org.sklsft.connect.mvc.listeners;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 * 
 * From Omnifaces, special thanks to BalusC
 *
 */
public abstract class DefaultPhaseListener implements PhaseListener {

	private static final long serialVersionUID = -7252366571645029385L;
	private	transient PhaseId phaseId; // PhaseId is not serializable

	public DefaultPhaseListener(PhaseId phaseId) {
		this.phaseId = phaseId;
	}

	@Override
	public PhaseId getPhaseId() {
		return phaseId;
	}

	@Override
	public void afterPhase(PhaseEvent event) {
		// NOOP.
	}

	@Override
	public void beforePhase(PhaseEvent event) {
		// NOOP.
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		out.writeObject(phaseId.getName());
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		phaseId = PhaseId.phaseIdValueOf((String) in.readObject());
	}

}