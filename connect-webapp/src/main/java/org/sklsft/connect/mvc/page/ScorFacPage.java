package org.sklsft.connect.mvc.page;


public enum ScorFacPage {
    PUBLIC_INDEX("/public/index.jsf"),
    PRIVATE_FACULTY("/private/faculty/index.xhtml"),
    PRIVATE_ADMIN("/private/admin/index.xhtml");

    private String url;

    ScorFacPage(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}