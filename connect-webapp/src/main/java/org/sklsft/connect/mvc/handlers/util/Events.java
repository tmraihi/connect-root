package org.sklsft.connect.mvc.handlers.util;

import static org.sklsft.connect.mvc.handlers.util.Faces.getViewRoot;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;

import org.sklsft.connect.mvc.listeners.CallbackPhaseListener;
import org.sklsft.connect.mvc.listeners.DefaultPhaseListener;

/**
 * 
 * From Omnifaces, special thanks to BalusC
 *
 */
public final class Events {

	public static void subscribeToRequestAfterPhase(PhaseId phaseId, Callback.Void callback) {
		addRequestPhaseListener(createAfterPhaseListener(phaseId, Events.<PhaseEvent>wrap(callback)));
	}
	
	
	private static <A> Callback.WithArgument<A> wrap(final Callback.Void callback) {
		return new Callback.WithArgument<A>() {

			@Override
			public void invoke(A argument) {
				callback.invoke();
			}
		};
	}
	
	
	public static void addRequestPhaseListener(PhaseListener listener) {
		CallbackPhaseListener.add(listener);
	}
	
	
	public static void subscribeToViewEvent(Class<? extends SystemEvent> type, SystemEventListener listener) {
		getViewRoot().subscribeToViewEvent(type, listener);
	}
	
	
	private static PhaseListener createAfterPhaseListener(PhaseId phaseId, final Callback.WithArgument<PhaseEvent> callback) {
		return new DefaultPhaseListener(phaseId) {

			private static final long serialVersionUID = 1L;

			@Override
			public void afterPhase(PhaseEvent event) {
				callback.invoke(event);
			}
		};
	}
}