package org.sklsft.connect.mvc.model.home;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.sklsft.connect.api.model.account.ov.AccountBasicView;
import org.sklsft.connect.api.model.organization.ov.InvitationView;
import org.sklsft.connect.api.model.organization.ov.OrganizationBasicView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class IndexView implements Serializable {

	private static final long serialVersionUID = -733357950280877174L;
	
	
	/*
	 * properties
	 */
	private AccountBasicView account;
	
	private List<OrganizationBasicView> myOrganizations = new ArrayList<>();
	private List<InvitationView> myInvitations = new ArrayList<>();
	
	private String token;	
	
	
	/*
	 * getters and setters
	 */
	public AccountBasicView getAccount() {
		return account;
	}
	public void setAccount(AccountBasicView account) {
		this.account = account;
	}
	public List<OrganizationBasicView> getMyOrganizations() {
		return myOrganizations;
	}
	public void setMyOrganizations(List<OrganizationBasicView> myOrganizations) {
		this.myOrganizations = myOrganizations;
	}
	public List<InvitationView> getMyInvitations() {
		return myInvitations;
	}
	public void setMyInvitations(List<InvitationView> myInvitations) {
		this.myInvitations = myInvitations;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
