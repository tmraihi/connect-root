package org.sklsft.connect.mvc.model.organization;

import java.io.Serializable;

import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class OrganizationCreationView implements Serializable {

	private static final long serialVersionUID = -1985728901926818898L;

	/*
	 * properties
	 */
	private OrganizationForm organizationForm;
	

	
	/*
	 * getters and setters
	 */
	public OrganizationForm getOrganizationForm() {
		return organizationForm;
	}

	public void setOrganizationForm(OrganizationForm organizationForm) {
		this.organizationForm = organizationForm;
	}
}
