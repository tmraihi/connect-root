package org.sklsft.connect.mvc.aspect;

public interface RedirectExceptionHandler {

	void redirectOnAccessDenied();
	
	void redirectOnMissingResource();	

	void redirectOnInvalidToken();	

	void redirectOnTokenExpired();

	void redirectOnAccountLocked();

	void redirectOnAccountDeleted();
	
	void redirectOnException();	
}
