package org.sklsft.connect.mvc.security;

import javax.inject.Inject;

import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.mvc.controller.BaseController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Component;


/**
 * the class responsible for providing a {@link SecurityContext} from one or no token.<br/>
 * if no token is used, a public context is created.
 * @author Nicolas Thibault
 *
 */
@Component
public class ConnectSecurityContextProvider extends BaseController {
	
	@Inject 
	private CookieManager cookieManager;
	
	public void provideSecurityContext(TokenView tokenView) {
			
		Authentication authentication = new ConnectAuthentication(tokenView);
		SecurityContext context = new SecurityContextImpl();
		context.setAuthentication(authentication);
		
		SecurityContextHolder.setContext(context);
		
		cookieManager.setCookie("token", tokenView.getToken(), 3600);
		
	}
	
	public void clearSecurityContext() {
		SecurityContextHolder.clearContext();
	}
}
