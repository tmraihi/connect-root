package org.sklsft.connect.mvc.aspect;

import javax.inject.Inject;

import org.sklsft.commons.mvc.redirection.RedirectionHandler;
import org.sklsft.connect.mvc.controller.authentication.AuthenticationController;
import org.sklsft.connect.mvc.controller.home.IndexController;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


/**
 * Defines what happens when an exception is thrown after a page load<br>
 * Implements {@link ApplicationContextAware} since {@link IndexController} has a request scope and cannot be injected directly
 * 
 * @author NTHI
 *
 */
public class RedirectExceptionHandlerImpl implements RedirectExceptionHandler, ApplicationContextAware {

	@Inject
	private RedirectionHandler redirectionHandler;
	
	
	private ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		
	}
	

	

	@Override
	public void redirectOnAccessDenied() {
		redirectionHandler.redirect(ExceptionPages.ACCES_DENIED.getUrl());

	}

	@Override
	public void redirectOnMissingResource() {
		redirectionHandler.redirect(ExceptionPages.MISSING_RESOURCE.getUrl());

	}


	@Override
	public void redirectOnInvalidToken() {
		logout();
	}

	@Override
	public void redirectOnTokenExpired() {
		logout();
	}

	@Override
	public void redirectOnAccountLocked() {
		logout();
	}

	@Override
	public void redirectOnAccountDeleted() {
		logout();
	}	
	
	@Override
	public void redirectOnException() {
		redirectionHandler.redirect(ExceptionPages.EXCEPTION.getUrl());
	}
	
	
	private void logout() {		
		applicationContext.getBean(AuthenticationController.class).logout();		
	}
}
