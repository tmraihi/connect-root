package org.sklsft.connect.mvc.listeners;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LifeCycleListener implements PhaseListener {

	private static final long serialVersionUID = 1L;
	
	
	private static final Logger logger = LoggerFactory
			.getLogger(LifeCycleListener.class);

	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

	public void beforePhase(PhaseEvent event) {
		logger.debug("START PHASE " + event.getPhaseId());
	}

	public void afterPhase(PhaseEvent event) {
		logger.debug("END PHASE " + event.getPhaseId());
	}

}