package org.sklsft.connect.mvc.aspect;

public enum ExceptionPages {

	ACCES_DENIED("/public/errors/403.jsf?faces-redirect=true"),

	MISSING_RESOURCE("/public/errors/404.jsf?faces-redirect=true"),

	EXCEPTION("/public/errors/500.jsf?faces-redirect=true");

	private ExceptionPages(String url) {
		this.url = url;
	}

	/*
	 * properties
	 */
	private String url;

	/*
	 * getters
	 */
	public String getUrl() {
		return url;
	}
}
