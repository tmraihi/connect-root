package org.sklsft.connect.mvc.handlers.util;

import java.util.Map;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

/**
 * 
 * From Omnifaces, special thanks to BalusC
 *
 */
public class Faces {
	
	public static FacesContext getContext() {
		return FacesContext.getCurrentInstance();
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getContextAttribute(FacesContext context, String name) {
		return (T) context.getAttributes().get(name);
	}

	public static void setContextAttribute(FacesContext context, String name, Object value) {
		context.getAttributes().put(name, value);
	}
	
	
	public static Map<String, Object> getRequestMap(FacesContext context) {
		return context.getExternalContext().getRequestMap();
	}


	@SuppressWarnings("unchecked")
	public static <T> T getRequestAttribute(FacesContext context, String name) {
		return (T) getRequestMap(context).get(name);
	}
	

	public static void setRequestAttribute(FacesContext context, String name, Object value) {
		getRequestMap(context).put(name, value);
	}
	
	
	public static UIViewRoot getViewRoot() {
		return getContext().getViewRoot();
	}
}
