package org.sklsft.connect.mvc.listeners;

import static javax.faces.event.PhaseId.ANY_PHASE;
import static org.sklsft.connect.mvc.handlers.util.Faces.getContext;
import static org.sklsft.connect.mvc.handlers.util.Faces.getRequestAttribute;
import static org.sklsft.connect.mvc.handlers.util.Faces.setRequestAttribute;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.sklsft.connect.mvc.handlers.tags.SkipValidation;

/**
 * 
 * From Omnifaces, special thanks to BalusC
 * Used by {@link SkipValidation}
 *
 */
public class CallbackPhaseListener implements PhaseListener {

	// Constants ------------------------------------------------------------------------------------------------------

	private static final long serialVersionUID = 3611407485061585042L;

	// Actions --------------------------------------------------------------------------------------------------------

	@Override
	public PhaseId getPhaseId() {
		return ANY_PHASE;
	}

	@Override
	public void beforePhase(final PhaseEvent event) {
		for (PhaseListener phaseListener : getCallbackPhaseListenersForEvent(event)) {
			phaseListener.beforePhase(event);
		}
	}

	@Override
	public void afterPhase(PhaseEvent event) {
		for (PhaseListener phaseListener : getCallbackPhaseListenersForEvent(event)) {
			phaseListener.afterPhase(event);
		}
	}

	// Utility --------------------------------------------------------------------------------------------------------

	/**
	 * Adds the given phase listener to the current request scope.
	 * @param phaseListener The phase listener to be added to the current request scope.
	 */
	public static void add(PhaseListener phaseListener) {
		getCallbackPhaseListeners(getContext(), true).add(phaseListener);
	}

	/**
	 * Removes the given phase listener from the current request scope.
	 * @param phaseListener The phase listener to be removed from the current request scope.
	 * @return <code>true</code> if the current request scope indeed contained the given phase listener.
	 */
	public static boolean remove(PhaseListener phaseListener) {
		Set<PhaseListener> phaseListeners = getCallbackPhaseListeners(getContext(), false);
		return phaseListeners == null ? false : phaseListeners.remove(phaseListener);
	}

	// Helpers --------------------------------------------------------------------------------------------------------


	private static Set<PhaseListener> getCallbackPhaseListeners(FacesContext context, boolean create) {
		Set<PhaseListener> set = getRequestAttribute(context, CallbackPhaseListener.class.getName());

		if (set == null && create) {
			set = new HashSet<>(1);
			setRequestAttribute(context, CallbackPhaseListener.class.getName(), set);
		}

		return set;
	}

	private static Set<PhaseListener> getCallbackPhaseListenersForEvent(PhaseEvent event) {
		Set<PhaseListener> phaseListeners = getCallbackPhaseListeners(event.getFacesContext(), false);

		if (phaseListeners == null) {
			return Collections.emptySet();
		}

		Set<PhaseListener> phaseListenersForEvent = new HashSet<>();

		for (PhaseListener phaseListener : phaseListeners) {
			if (isPhaseMatch(event, phaseListener.getPhaseId())) {
				phaseListenersForEvent.add(phaseListener);
			}
		}

		return Collections.unmodifiableSet(phaseListenersForEvent);
	}

	private static boolean isPhaseMatch(PhaseEvent event, PhaseId phaseId) {
		return ANY_PHASE.equals(phaseId) || event.getPhaseId().equals(phaseId);
	}

}