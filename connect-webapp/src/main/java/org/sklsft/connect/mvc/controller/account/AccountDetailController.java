package org.sklsft.connect.mvc.controller.account;

import javax.inject.Inject;

import org.primefaces.event.FileUploadEvent;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.account.of.AccountPasswordForm;
import org.sklsft.connect.mvc.aspect.PageLoad;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.account.AccountDetailView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class AccountDetailController extends BaseController {

	/*
	 * the view(s) handled by the controller
	 */
	@Inject
	private AccountDetailView view;


	/*
	 * the service(s) used by the controller
	 */
	@Inject
	private AccountService accountService;
	
	

	private void refreshMyAccount() {
		view.setAccount(accountService.retrieveMyFullAccount());
	}

	
	@PageLoad
	public void load() {
		refreshMyAccount();
	}


	@AjaxMethod(value="personalData.update")
	public void updateMyPersonalData() {
		accountService.updateMyPersonalData(view.getAccount().getPersonalData());
	}


	@AjaxMethod(value="password.update")
	public void updateMyPassword() {
		accountService.updateMyPassword(view.getAccountPasswordForm());
		view.setAccountPasswordForm(new AccountPasswordForm()) ;
	}
	

	@AjaxMethod(value="email.update")
	public void updateMyMail() {
		accountService.updateMyMail(view.getAccount().getEmailForm());
	}
	

	@AjaxMethod(value="account.logo.upload")
	public void uploadMyAccountLogo(FileUploadEvent event)  {
		accountService.uploadMyAccountLogo(event.getFile().getContents());
	}
}
