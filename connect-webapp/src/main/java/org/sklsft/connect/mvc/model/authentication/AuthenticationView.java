package org.sklsft.connect.mvc.model.authentication;

import org.sklsft.connect.api.model.authentication.of.LoginForm;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class AuthenticationView {
	
	/*
	 * properties
	 */
	private LoginForm loginForm = new LoginForm();

	
	/*
	 * getters and setters
	 */
	public LoginForm getLoginForm() {
		return loginForm;
	}

	public void setLoginForm(LoginForm loginForm) {
		this.loginForm = loginForm;
	}
}
