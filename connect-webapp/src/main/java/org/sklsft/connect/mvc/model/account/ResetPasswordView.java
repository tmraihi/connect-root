package org.sklsft.connect.mvc.model.account;

import org.sklsft.connect.api.model.account.of.ResetPasswordForm;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class ResetPasswordView {
	/*
	 * properties
	 */
	private ResetPasswordForm resetPasswordForm = new ResetPasswordForm();

	/*
	 * getters and setters
	 */
	public ResetPasswordForm getResetPasswordForm() {
		return resetPasswordForm;
	}

	public void setResetPasswordForm(ResetPasswordForm resetPasswordForm) {
		this.resetPasswordForm = resetPasswordForm;
	}
}
