package org.sklsft.connect.mvc.controller.organization;

import java.io.IOException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.interfaces.organization.OrganizationMembershipService;
import org.sklsft.connect.api.interfaces.organization.OrganizationService;
import org.sklsft.connect.api.model.organization.of.GuestForm;
import org.sklsft.connect.api.model.organization.of.OrganizationCodeForm;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.sklsft.connect.mvc.aspect.PageLoad;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.home.IndexView;
import org.sklsft.connect.mvc.model.organization.OrganizationDetailView;
import org.sklsft.social.util.pages.Page;
import org.sklsft.social.util.roles.OrganizationRole;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class OrganizationDetailController extends BaseController {

	/*
	 * the view(s) handled by the controller
	 */
	@Inject
	private OrganizationDetailView view;

	@Inject
	private IndexView indexView;

	/*
	 * the service(s) used by the controller
	 */
	@Inject
	private OrganizationService organizationService;

	@Inject
	private AccountService accountService;

	@Inject
	private OrganizationMembershipService organizationMembershipService;
	

	@PageLoad
	public void load() {
		refresh(getCurrentOrganizationId());
	}
	

	private void refresh(Long id) {
		refreshOrganizationData(id);
		refreshMembers(id);
		refreshGuests(id);
		refreshRoles();
	}
	

	private void refreshOrganizationData(Long id) {
		view.setOrganizationFullView(organizationService.retrieveMyOrganization(id));
		view.setCodeEditable(false);
	}

	private void refreshMembers(Long id) {
		view.setMembers(organizationMembershipService.getMyOrganizationMembers(id));
	}
	

	private void refreshGuests(Long id) {
		view.setGuests(organizationMembershipService.getMyOrganizationGuests(id));
	}
	

	private void refreshRoles() {
		view.setRoles(new ArrayList<SelectItem>());
		view.getRoles().add(new SelectItem(OrganizationRole.ADMIN, OrganizationRole.ADMIN.getLabel()));
		view.getRoles().add(new SelectItem(OrganizationRole.MEMBER, OrganizationRole.MEMBER.getLabel()));
	}

	
	@AjaxMethod(value = "organization.update")
	public void updateOrganization() {
		
		Long id = getCurrentOrganizationId();
		OrganizationForm form = view.getOrganizationFullView().getOrganizationForm();
		organizationService.updateMyOrganization(id, form);
		refreshOrganizationData(id);
		indexView.setAccount(accountService.retrieveMyAccount());
	}
	

	@AjaxMethod(value = "organization.code.update")
	public void updateOrganizationCode() {

		Long id = getCurrentOrganizationId();
		OrganizationCodeForm code = view.getOrganizationFullView().getOrganizationCodeForm();
		organizationService.updateMyOrganizationIdentifier(id, code);
		refreshOrganizationData(id);

		view.setCodeEditable(false);
	}
	

	public void checkCodeAlreadyExists() {

		OrganizationCodeForm organizationCodeForm = view.getOrganizationFullView().getOrganizationCodeForm();
		boolean exist = organizationService.existsOrganization(organizationCodeForm.getCode());
		this.view.setCodeAlreadyExists(exist);
	}
	

	@AjaxMethod(value = "organization.updateMember")
	public void updateMember() {

		Long id = getCurrentOrganizationId();
		Long memberId = getTargetMemberId();
		
		organizationMembershipService.updateMember(id, memberId, view.getTargetMemberForm());

		refreshMembers(id);
	}


	@AjaxMethod(value = "organization.removeMember")
	public void removeMember() {
		
		Long id = getCurrentOrganizationId();
		Long memberId = getTargetMemberId();

		organizationMembershipService.deleteMember(id, memberId);

		refreshMembers(id);
	}
	

	@AjaxMethod(value = "organization.logo.upload")
	public void uploadOrganizationLogo() throws IOException {
		
		Long id = getCurrentOrganizationId();
		organizationService.uploadMyOrganizationLogo(id, IOUtils.toByteArray(view.getLogo().getInputStream()));
	}
	

	public void prepareInviteContact() {

		GuestForm inviteMemberForm = new GuestForm();
		inviteMemberForm.getMemberForm().setRole(OrganizationRole.MEMBER);

		view.setInviteMemberForm(inviteMemberForm);
	}
	

	@AjaxMethod(value = "organization.inviteContact")
	public void inviteContact() {
		
		Long id = getCurrentOrganizationId();
		
		this.organizationMembershipService.inviteMember(id, view.getInviteMemberForm());
		refreshGuests(id);
	}
	

	@AjaxMethod(value = "organization.cancelInviteContact")
	public void cancelInviteContact(String email) {
		
		Long id = getCurrentOrganizationId();
		
		this.organizationMembershipService.cancelInviteMember(id, email);
		refreshGuests(id);
	}
	

	public void deleteOrganization() {
		
		executeAjaxMethod("organization.delete", new AjaxMethodTemplate() {
			@Override
			public Object execute() {
				Long id = getCurrentOrganizationId();
				organizationService.deleteMyOrganization(id);
				return null;
			}

			@Override
			public void redirectOnComplete(Object result) {
				redirect(Page.PRIVATE_INDEX.getUrl());
			}
		});
	}
	
	
	private Long getCurrentOrganizationId() {
		return view.getOrganizationFullView().getId();
	}
	
	
	private Long getTargetMemberId() {
		return view.getTargetMember().getId();
	}
}
