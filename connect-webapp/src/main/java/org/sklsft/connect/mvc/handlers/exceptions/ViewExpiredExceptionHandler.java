package org.sklsft.connect.mvc.handlers.exceptions;

import java.io.IOException;
import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import javax.servlet.http.HttpServletResponse;

public class ViewExpiredExceptionHandler extends ExceptionHandlerWrapper {

	private ExceptionHandler wrapped;
	 
    public ViewExpiredExceptionHandler(ExceptionHandler wrapped) {
        this.wrapped = wrapped;
    }
 
    @Override
    public ExceptionHandler getWrapped() {
        return this.wrapped;
    }
 
	@Override
	public void handle() throws FacesException {
		
		boolean callWrapped = true;
		
		for (Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator(); i.hasNext();) {
			ExceptionQueuedEvent event = i.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
			Throwable t = context.getException();
			
			if (t instanceof ViewExpiredException) {
				
				FacesContext facesContext = FacesContext.getCurrentInstance();
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				
				try {
					HttpServletResponse response = (HttpServletResponse)externalContext.getResponse();
					response.sendRedirect(externalContext.getRequestContextPath() + "/private/index.xhtml");
					facesContext.responseComplete();
					callWrapped = false;
				} catch (IOException e) {
					
				}
			}
		}

		if (callWrapped) {
			getWrapped().handle();
		}
	}
}
