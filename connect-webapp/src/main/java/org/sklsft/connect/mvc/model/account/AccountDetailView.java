package org.sklsft.connect.mvc.model.account;

import java.io.Serializable;

import javax.servlet.http.Part;

import org.sklsft.connect.api.model.account.of.AccountPasswordForm;
import org.sklsft.connect.api.model.account.ov.AccountFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class AccountDetailView implements Serializable {

	private static final long serialVersionUID = -733357950280877174L;
	
	
	/*
	 * properties
	 */
	private AccountFullView account;
	private AccountPasswordForm accountPasswordForm = new AccountPasswordForm();
	
	private Part logo;
	
	
	
	/*
	 * getters and setters
	 */
	public AccountFullView getAccount() {
		return account;
	}
	public void setAccount(AccountFullView account) {
		this.account = account;
	}
	public AccountPasswordForm getAccountPasswordForm() {
		return accountPasswordForm;
	}
	public void setAccountPasswordForm(AccountPasswordForm accountPasswordForm) {
		this.accountPasswordForm = accountPasswordForm;
	}
	public Part getLogo() {
		return logo;
	}
	public void setLogo(Part logo) {
		this.logo = logo;
	}
}
