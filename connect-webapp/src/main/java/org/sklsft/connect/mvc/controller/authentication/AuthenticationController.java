package org.sklsft.connect.mvc.controller.authentication;

import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.sklsft.commons.api.exception.ApplicationException;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.interfaces.authentication.AuthenticationService;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.mvc.controller.BaseController;
import org.sklsft.connect.mvc.model.authentication.AuthenticationView;
import org.sklsft.connect.mvc.model.home.IndexView;
import org.sklsft.connect.mvc.page.ScorFacPage;
import org.sklsft.connect.mvc.security.SecurityController;
import org.sklsft.social.util.pages.Page;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class AuthenticationController extends BaseController {

    /*
     * the view(s) handled by the controller
     */
    @Inject
    private AuthenticationView authenticationView;

    @Inject
    private IndexView indexView;


    /*
     * the service(s) used by the controller
     */
    @Inject
    private AuthenticationService authenticationService;

    @Inject
    private AccountService accountService;

    @Inject
    private SecurityController securityController;


    public void authenticate() {

        try {
            TokenView tokenView = authenticationService
                    .authenticate(authenticationView.getLoginForm());
            securityController.provideSecurityContext(tokenView);

            indexView.setAccount(accountService.retrieveMyAccount());

            accessPrivateSite(tokenView);

        } catch (ApplicationException e) {
            displayError(e.getMessage());
            logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage());
        } catch (Exception e) {
            displayError("account.authenticate.failure");
            logger.error("failed : " + e.getClass().getSimpleName() + " - " + e.getMessage());
        }
    }


    public void accessPrivateSite(TokenView tokenView) {

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

        //pk utiliser targetUrl ?
        String targetUrl = (String) externalContext.getSessionMap().get("targetUrl");

        String userRole = tokenView.getUserRole();
        if (StringUtils.isEmpty(userRole)) {
            redirect(ScorFacPage.PUBLIC_INDEX.getUrl());
        } else if (userRole.equals("USER_CANDIDATE_ROLE")) {
            redirect(ScorFacPage.PRIVATE_FACULTY.getUrl());
        } else if (userRole.equals("USER_ADMIN_ROLE")) {
            redirect(ScorFacPage.PRIVATE_ADMIN.getUrl());
        } else {
            redirect(ScorFacPage.PUBLIC_INDEX.getUrl());
        }


    }

    /**
     * if (targetUrl == null || !targetUrl.contains("/private/")) {
     * redirect(ScorFacPage.PUBLIC_INDEX.getUrl());
     * } else {
     * try {
     * externalContext.redirect(targetUrl);
     * <p>
     * } catch (IOException e) {
     * redirect(ScorFacPage.PUBLIC_INDEX.getUrl());
     * }
     * }
     **/


    public void logout() {

        securityController.clearSecurityContext();

        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        redirect(Page.PUBLIC_INDEX.getUrl());
    }
}
