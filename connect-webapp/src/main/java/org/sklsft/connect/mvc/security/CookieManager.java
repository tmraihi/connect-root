package org.sklsft.connect.mvc.security;

import java.util.Arrays;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@Component
public class CookieManager {

	public void setCookie(String name, String value, int expiryInSeconds) {		
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

		Cookie cookie = getCookie(name);
		if (cookie != null) {
			cookie.setValue(value);
		} else {
			cookie = new Cookie(name, value);
		}
		
		cookie.setPath("/");  //((HttpServletRequest) context.getRequest()).getContextPath().toString());
		cookie.setMaxAge(expiryInSeconds);

		((HttpServletResponse) context.getResponse()).addCookie(cookie); 
	}
	
	public void eraseCookie(String name) {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

		Cookie cookie = getCookie(name);
		if (cookie != null) {
			cookie.setValue("");
			cookie.setPath("/");
			cookie.setMaxAge(0);
			((HttpServletResponse) context.getResponse()).addCookie(cookie); 
		}
	}

	public Cookie getCookie(String name) {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

		return Arrays.stream(request.getCookies())
				.filter(cookie -> cookie.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
}


