package org.sklsft.connect.bl.account;

import javax.inject.Inject;
import javax.mail.MessagingException;

import org.sklsft.commons.api.exception.TechnicalError;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.rest.security.credentials.validator.SecurityCredentialsValidator;
import org.sklsft.commons.rest.security.tokens.encoder.TokenEncoder;
import org.sklsft.connect.api.exception.account.EmailAlreadyActivatedException;
import org.sklsft.connect.api.exception.account.EmailNotFoundException;
import org.sklsft.connect.api.exception.account.ValidationEmailExpiredException;
import org.sklsft.connect.api.exception.authentication.TokenExpiredException;
import org.sklsft.connect.api.interfaces.account.AccountService;
import org.sklsft.connect.api.model.account.of.AccountCreationForm;
import org.sklsft.connect.api.model.account.of.AccountPasswordForm;
import org.sklsft.connect.api.model.account.of.MailForm;
import org.sklsft.connect.api.model.account.of.PersonalDataForm;
import org.sklsft.connect.api.model.account.of.ResetPasswordForm;
import org.sklsft.connect.api.model.account.ov.AccountBasicView;
import org.sklsft.connect.api.model.account.ov.AccountFullView;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.bc.mail.manager.account.ResetPasswordMailManager;
import org.sklsft.connect.bc.mail.manager.account.ValidationMailManager;
import org.sklsft.connect.bc.mapper.account.AccountBasicViewMapper;
import org.sklsft.connect.bc.mapper.account.AccountCreationFormMapper;
import org.sklsft.connect.bc.mapper.account.AccountFullViewMapper;
import org.sklsft.connect.bc.mapper.account.PersonalDataFormMapper;
import org.sklsft.connect.bc.security.UserContextHolder;
import org.sklsft.connect.bc.security.credentials.UserSecurityCredentials;
import org.sklsft.connect.bc.security.token.TokenFactory;
import org.sklsft.social.api.exception.state.account.AccountAlreadyUsedException;
import org.sklsft.social.bc.processor.account.UserAccountProcessor;
import org.sklsft.social.bc.statemanager.account.UserAccountStateManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationStateManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.file.DefaultLogoManager;
import org.sklsft.social.repository.file.FileManager;
import org.sklsft.social.util.mail.exception.MailSendFailureException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountServiceImpl implements AccountService {

	@Inject
	private UserContextHolder userContextHolder;

	@Inject
	private UserAccountDao userAccountDao;

	@Inject
	private UserAccountProcessor userAccountProcessor;
	
	@Inject
	private AccountFullViewMapper accountFullViewMapper;
	
	@Inject
	private AccountBasicViewMapper accountBasicViewMapper;

	@Inject
	private TokenEncoder<UserSecurityCredentials> credentialsEncoder;

	@Inject
	private SecurityCredentialsValidator<UserSecurityCredentials> credentialsValidator;

	@Inject
	private TokenFactory tokenFactory;

	@Inject
	private PersonalDataFormMapper accountBasicDataFormMapper;

	@Inject
	private ValidationMailManager validationMailManager;

	@Inject
	private ResetPasswordMailManager resetPasswordMailManager;

	@Inject
	private OrganizationStateManager organizationStateManager;

	@Inject
	private FileManager uploadManager;
	
	@Inject
	private DefaultLogoManager defaultLogoManager;
	
	@Inject
	private AccountCreationFormMapper accountCreationFormMapper;
	
	@Inject
	private UserAccountStateManager userAccountStateManager;
	
	@Inject
	private PasswordEncoder passwordEncoder;
	
		
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public TokenView createAccount(AccountCreationForm form)
			throws AccountAlreadyUsedException {
		
		UserAccount userAccount = accountCreationFormMapper.mapTo(form, new UserAccount());
		
		userAccountStateManager.checkCanSave(userAccount);
		
		userAccountProcessor.save(userAccount);

		try {
			validationMailManager.sendActivationMail(userAccount);
		} catch (MessagingException e) {
			throw new TechnicalError("validationEmail.send.failure", e);
		}

		return tokenFactory.buildTokenView(userAccount);
	}	
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public TokenView activateEmail(String token)
			throws EmailAlreadyActivatedException,
			ValidationEmailExpiredException {
		UserSecurityCredentials credentials = credentialsEncoder.decode(token);

		try {
			credentialsValidator.validateCredentials(credentials);
		} catch (TokenExpiredException e) {
			throw new ValidationEmailExpiredException(
					"validationEmail.expired", e);
		}

		UserAccount userAccount = userAccountDao.load(credentials
				.getAccountId());

		if (userAccount.getEmailValidated()) {
			throw new EmailAlreadyActivatedException(
					"account.alreadyValidated");
		} else {
			userAccount.setEmailValidated(true);
		}

		return tokenFactory.buildTokenView(userAccount);

	}
	

	@Override
	@Transactional(readOnly = true)
	public void resendValidationMail() {

		UserAccount userAccount = userContextHolder.getCurrentAccount();

		try {
			validationMailManager.sendActivationMail(userAccount);
		} catch (MessagingException e) {
			throw new TechnicalError("validationEmail.send.failure", e);
		}
	}
	

	@Override
	@Transactional(readOnly = true)
	public AccountBasicView retrieveMyAccount() {
		UserAccount userAccount = userContextHolder.getCurrentAccount();

		AccountBasicView accountView = accountBasicViewMapper.mapFrom(
				new AccountBasicView(), userAccount);
		return accountView;
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public AccountFullView retrieveMyFullAccount() {
		UserAccount userAccount = userContextHolder.getCurrentAccount();

		AccountFullView accountView = accountFullViewMapper.mapFrom(
				new AccountFullView(), userAccount);
		return accountView;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateMyPersonalData(PersonalDataForm form) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();

		userAccount = accountBasicDataFormMapper.mapTo(form, userAccount);
	}
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void uploadMyAccountLogo(byte[] data) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		try {
			uploadManager.saveFile(data, userAccount);
		} catch (Exception e) {
			throw new TechnicalError("account.logo.upload.failure", e);
		}

	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateMyMail(MailForm form) {
		
		UserAccount userAccount = userContextHolder.getCurrentAccount();

		if (userAccount.getEmail().equalsIgnoreCase(form.getEmail())) {
			return;
		}
		if (userAccountDao.exists(form.getEmail())) {
			throw new AccountAlreadyUsedException("account.alreadyUsed");
		}

		userAccount.setEmail(form.getEmail());
		userAccount.setEmailValidated(false);

		try {
			validationMailManager.sendActivationMail(userAccount);
		} catch (MessagingException e) {
			throw new TechnicalError("validationEmail.send.failure", e);
		}
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateMyPassword(AccountPasswordForm accountPasswordForm) {

		UserAccount userAccount = userContextHolder.getCurrentAccount();

		userAccount.setPasswordHash(passwordEncoder.encode(accountPasswordForm
				.getNewPassword()));

	}
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void sendResetPassword(MailForm emailForm)
			throws EmailNotFoundException, MailSendFailureException {

		UserAccount userAccount;
		try {
			userAccount = userAccountDao.find(emailForm.getEmail());
			resetPasswordMailManager.sendResetPasswordMail(userAccount);

		} catch (ObjectNotFoundException e) {
			throw new EmailNotFoundException("password.reset.emailnotfound");
		} catch (MessagingException e) {
			throw new MailSendFailureException("password.reset.sent.failure", e);
		}

	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public TokenView resetPassword(ResetPasswordForm resetPasswordForm) {
		
		UserSecurityCredentials credentials = credentialsEncoder.decode(resetPasswordForm.getToken());

		try {
			credentialsValidator.validateCredentials(credentials);
		} catch (TokenExpiredException e) {
			throw new ValidationEmailExpiredException(
					"validationEmail.expired", e);
		}

		UserAccount userAccount = userAccountDao.load(credentials
				.getAccountId());

		userAccount.setPasswordHash(this.passwordEncoder
				.encode(resetPasswordForm.getNewPassword()));

		return tokenFactory.buildTokenView(userAccount);		

	}
}
