package org.sklsft.connect.bl.organization;

import javax.inject.Inject;

import org.sklsft.connect.api.interfaces.organization.OrganizationPublicService;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;
import org.sklsft.social.repository.file.DefaultLogoManager;
import org.sklsft.social.repository.file.FileManager;
import org.sklsft.social.repository.file.exception.FileNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrganizationPublicServiceImpl implements OrganizationPublicService {
	
	@Inject
	private OrganizationDao organizationDao;
	
	@Inject
	private FileManager fileManager;
	
	@Inject
	private DefaultLogoManager defaultLogoManager;
	
	
	
	@Override
	@Transactional(readOnly = true)
	public byte[] getOrganizationLogo(Long id) {

		Organization organization = organizationDao.load(id);
		
		try {
			return fileManager.retrieveFile(organization);
		} catch (FileNotFoundException e) {
			return defaultLogoManager.getDefaultOrganizationLogo();
		}
	}
}
