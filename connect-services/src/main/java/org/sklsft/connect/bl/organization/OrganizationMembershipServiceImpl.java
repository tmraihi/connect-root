package org.sklsft.connect.bl.organization;

import java.util.List;

import javax.inject.Inject;

import org.sklsft.connect.api.interfaces.organization.OrganizationMembershipService;
import org.sklsft.connect.api.model.organization.of.GuestForm;
import org.sklsft.connect.api.model.organization.of.MemberForm;
import org.sklsft.connect.api.model.organization.ov.GuestView;
import org.sklsft.connect.api.model.organization.ov.MemberView;
import org.sklsft.connect.bc.mapper.organization.GuestFormMapper;
import org.sklsft.connect.bc.mapper.organization.MemberFormMapper;
import org.sklsft.connect.bc.retriever.organization.OrganizationMembershipRetriever;
import org.sklsft.connect.bc.rights.organization.OrganizationRightsManager;
import org.sklsft.connect.bc.security.UserContextHolder;
import org.sklsft.social.api.exception.state.organization.ContactAlreadyMemberException;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationGuestDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationMemberDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrganizationMembershipServiceImpl implements OrganizationMembershipService {
	
	@Inject
	private UserContextHolder userContextHolder;
	
	@Inject
	private OrganizationRightsManager organizationRightsManager;
	
	@Inject
	private OrganizationMemberDao organizationMemberDao;
	
	@Inject
	private OrganizationGuestDao organizationGuestDao;
	
	@Inject
	private OrganizationDao organizationDao;
	
	@Inject
	private OrganizationMembershipRetriever membershipRetriever;
	
	@Inject
	private MemberFormMapper memberFormMapper;
	
	@Inject
	private GuestFormMapper guestFormMapper;
	
	@Inject
	private UserAccountDao userAccountDao;
	
	
	
	
	@Override
	@Transactional(readOnly=true)
	public List<MemberView> getMyOrganizationMembers(Long id) {		
		UserAccount userAccount = userContextHolder.getCurrentAccount();		
		Organization organization = organizationDao.load(id);		
		organizationRightsManager.checkAccessEnabled(userAccount, organization);		
		return membershipRetriever.getMembers(id);
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateMember(Long organizationId, Long accountId, MemberForm form) {		
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(organizationId);
		organizationRightsManager.checkUpdateEnabled(userAccount, organization);		
		OrganizationMember organizationMember = organizationMemberDao.loadOrganizationMemberByOrganizationIdAndUserId(organizationId, accountId);
		organizationMember = memberFormMapper.mapTo(form, organizationMember);
		
	}
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteMember(Long organizationId, Long accountId) {		
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(organizationId);
		organizationRightsManager.checkUpdateEnabled(userAccount, organization);		
		OrganizationMember organizationMember = organizationMemberDao.loadOrganizationMemberByOrganizationIdAndUserId(organizationId, accountId);
		organizationMemberDao.delete(organizationMember);
	}

	
	@Override
	@Transactional(readOnly=true)
	public List<GuestView> getMyOrganizationGuests(Long id) {		
		UserAccount userAccount = userContextHolder.getCurrentAccount();		
		Organization organization = organizationDao.load(id);		
		organizationRightsManager.checkAccessEnabled(userAccount, organization);		
		return membershipRetriever.getGuests(id);
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void inviteMember(Long organizationId, GuestForm form) {		
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(organizationId);
		organizationRightsManager.checkUpdateEnabled(userAccount, organization);		
		UserAccount target = userAccountDao.findOrNull(form.getMailForm().getEmail());		
		
		if (organizationGuestDao.exists(organization.getCode(), target.getEmail())) {
			throw new ContactAlreadyMemberException("contact.alreadyInvited");
		}
		
		OrganizationGuest guest = new OrganizationGuest();
		guest.setOrganization(organization);
		guest.setUserAccount(target);
		guest.setEmail(form.getMailForm().getEmail());		
		guest = guestFormMapper.mapTo(form, guest);		
		organizationGuestDao.save(guest);
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void cancelInviteMember(Long organizationId, String eMail) {		
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(organizationId);
		organizationRightsManager.checkUpdateEnabled(userAccount, organization);		
		OrganizationGuest guest = organizationGuestDao.find(organization.getCode(), eMail);		
		organizationGuestDao.delete(guest);
	}
	
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void acceptJoinOrganization(Long id) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(id);
		OrganizationGuest guest = organizationGuestDao.findByOrganizationAndUser(id, userAccount.getId());
		OrganizationMember organizationMember = new OrganizationMember();
		organizationMember.setOrganization(organization);
		organizationMember.setUserAccount(userAccount);
		organizationMember.setRole(guest.getRole());
		organizationMemberDao.save(organizationMember);
		organizationGuestDao.delete(guest);
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void refuseJoinOrganization(Long id) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		OrganizationGuest guest = organizationGuestDao.findByOrganizationAndUser(id, userAccount.getId());
		organizationGuestDao.delete(guest);
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void leaveOrganization(Long id) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		OrganizationMember organizationMember = organizationMemberDao.loadOrganizationMemberByOrganizationIdAndUserId(id, userAccount.getId());
		organizationMemberDao.delete(organizationMember);
	}
}
