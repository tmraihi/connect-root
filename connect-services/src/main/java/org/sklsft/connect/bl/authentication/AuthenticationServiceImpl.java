package org.sklsft.connect.bl.authentication;

import javax.inject.Inject;

import org.sklsft.connect.api.exception.authentication.AccountLockedException;
import org.sklsft.connect.api.exception.authentication.AuthenticationException;
import org.sklsft.connect.api.interfaces.authentication.AuthenticationService;
import org.sklsft.connect.api.model.authentication.of.LoginForm;
import org.sklsft.connect.api.model.authentication.ov.TokenView;
import org.sklsft.connect.bc.security.token.TokenFactory;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
	
	@Inject
	private TokenFactory tokenFactory;
	
	@Inject
	private UserAccountDao userAccountDao;

	@Inject
	private PasswordEncoder passwordEncoder;


	@Override
	@Transactional(value="transactionManager", readOnly=true)
	public TokenView authenticate(LoginForm form) {

		UserAccount user = null;

		try {

			user = userAccountDao.find(form.getLogin());

		} catch (Exception e) {
			throw new AuthenticationException("account.authenticationFailure", e);
		}
		
		if (!passwordEncoder.matches(form.getPassword(), user.getPasswordHash())) {
			throw new AuthenticationException("account.authenticationFailure");
		}

		if (!user.getActivated()) {
			throw new AccountLockedException("account.locked");
		}

		return tokenFactory.buildTokenView(user);
	}
}
