package org.sklsft.connect.bl.organization;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.sklsft.commons.api.exception.ApplicationException;
import org.sklsft.commons.api.exception.TechnicalError;
import org.sklsft.connect.api.interfaces.organization.OrganizationService;
import org.sklsft.connect.api.model.organization.of.OrganizationCodeForm;
import org.sklsft.connect.api.model.organization.of.OrganizationForm;
import org.sklsft.connect.api.model.organization.ov.InvitationView;
import org.sklsft.connect.api.model.organization.ov.OrganizationBasicView;
import org.sklsft.connect.api.model.organization.ov.OrganizationFullView;
import org.sklsft.connect.bc.mapper.organization.InvitationViewMapper;
import org.sklsft.connect.bc.mapper.organization.OrganizationBasicViewMapper;
import org.sklsft.connect.bc.mapper.organization.OrganizationFormMapper;
import org.sklsft.connect.bc.mapper.organization.OrganizationFullViewMapper;
import org.sklsft.connect.bc.rights.organization.OrganizationRightsManager;
import org.sklsft.connect.bc.security.UserContextHolder;
import org.sklsft.social.bc.processor.organization.OrganizationProcessor;
import org.sklsft.social.bc.statemanager.organization.OrganizationStateManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationGuestDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationMemberDao;
import org.sklsft.social.repository.file.FileManager;
import org.sklsft.social.repository.file.exception.FileError;
import org.sklsft.social.util.roles.OrganizationRole;
import org.sklsft.social.util.text.FullTextNormalizer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Inject
	private UserContextHolder userContextHolder;

	@Inject
	private OrganizationDao organizationDao;
	
	@Inject
	private OrganizationMemberDao organizationMemberDao;
	
	@Inject
	private OrganizationGuestDao organizationGuestDao;
	
	@Inject
	private InvitationViewMapper invitationViewMapper;

	@Inject
	private OrganizationProcessor organizationProcessor;

	@Inject
	private OrganizationFormMapper organizationFormMapper;

	@Inject
	private OrganizationFullViewMapper organizationFullViewMapper;
	
	@Inject
	private OrganizationBasicViewMapper organizationBasicViewMapper;

	@Inject
	private OrganizationStateManager organizationStateManager;
	
	@Inject
	private OrganizationRightsManager organizationRightsManager;

	@Inject
	private FullTextNormalizer fullTextNormalizer;

	@Inject
	private FileManager fileManager;
	
	
	@Override
	@Transactional(readOnly = true)
	public List<OrganizationBasicView> retrieveMyOrganizations() {		
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		List<OrganizationMember> myOrganizationMembers = organizationMemberDao.loadListEagerlyFromUserAccount(userAccount.getId());
		List<OrganizationBasicView> result = new ArrayList<>(myOrganizationMembers.size());
		
		for (OrganizationMember member:myOrganizationMembers) {		
			result.add(organizationBasicViewMapper.mapFrom(member.getOrganization()));		}
		
		return result;
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public List<InvitationView> retrieveMyInvitations() {	
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		List<OrganizationGuest> invitations = organizationGuestDao.loadListEagerlyFromUserAccount(userAccount.getId());
		List<InvitationView> result = new ArrayList<>(invitations.size());
		
		for (OrganizationGuest guest:invitations) {		
			result.add(invitationViewMapper.mapFrom(guest));
		}
		
		return result;
	}
	

	@Override
	@Transactional(readOnly = true)
	public OrganizationFullView retrieveMyOrganization(Long id) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(id);
		organizationRightsManager.checkAccessEnabled(userAccount, organization);
		return organizationFullViewMapper.mapFrom(new OrganizationFullView(), organization);
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean existsOrganization(String code) {		
		return organizationDao.exists(code);		
	}
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Long createOrganization(OrganizationForm form) {

		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationFormMapper.mapTo(form, new Organization());
		OrganizationMember member = new OrganizationMember();		
		Long id = organizationProcessor.save(organization);
		
		member.setUserAccount(userAccount);
		member.setOrganization(organization);
		member.setRole(OrganizationRole.OWNER.name());
		
		organizationMemberDao.save(member);
		
		return id;
	}
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateMyOrganization(Long id, OrganizationForm form) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(id);
		organizationStateManager.checkCanUpdate(organization);
		organizationRightsManager.checkUpdateEnabled(userAccount, organization);
		organization = organizationFormMapper.mapTo(form, organization);
		organizationProcessor.update(organization);
	}
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateMyOrganizationIdentifier(Long id, OrganizationCodeForm organizationCodeForm) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(id);
		organizationRightsManager.checkUpdateEnabled(userAccount, organization);		
		organizationProcessor.updateIdentifier(organization, organizationCodeForm.getCode());		
	}
	

	@Override
	@Transactional(readOnly = true)
	public void uploadMyOrganizationLogo(Long id, byte[] bytes) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(id);
		organizationRightsManager.checkUpdateEnabled(userAccount, organization);

		try {
			fileManager.saveFile(bytes, organization);
		} catch (Exception e) {
			throw new FileError(TechnicalError.ERROR_UNKNOWN, e);
		}
	}


	@Override
	public void deleteMyOrganization(Long id) {
		UserAccount userAccount = userContextHolder.getCurrentAccount();
		Organization organization = organizationDao.load(id);
		organizationRightsManager.checkDeleteEnabled(userAccount, organization);		
		organizationProcessor.delete(organization);		
	}
}
