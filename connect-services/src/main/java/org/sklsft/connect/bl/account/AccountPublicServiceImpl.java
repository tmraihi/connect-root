package org.sklsft.connect.bl.account;

import javax.inject.Inject;

import org.sklsft.connect.api.interfaces.account.AccountPublicService;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.file.DefaultLogoManager;
import org.sklsft.social.repository.file.FileManager;
import org.sklsft.social.repository.file.exception.FileNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountPublicServiceImpl implements AccountPublicService {

	@Inject
	private UserAccountDao accountDao;
	
	@Inject
	private FileManager fileManager;
	
	@Inject
	private DefaultLogoManager defaultLogoManager;
		
	
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public byte[] getAccountLogo(Long id) {
		UserAccount userAccount = new UserAccount();
		userAccount.setId(id);
		try {
			return fileManager.retrieveFile(userAccount);
		} catch (FileNotFoundException e) {
			return defaultLogoManager.getDefaultAccountLogo();
		}

	}
}

